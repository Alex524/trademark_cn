﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.Admin.User
{
    [Export(typeof(IUserViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class UserViewModel : BindableBase, IUserViewModel
    {
        private readonly ICommand addCommand;
        private readonly ICommand refershCommand;
        private readonly ICommand removeCommand;
        private ILoginService LoginService;
        private IEventAggregator EventAggregator;
        private ObservableCollection<Account> accounts;
        private readonly ICommand editCommand;

        public InteractionRequest<EditUserNotification> EditUserRequest { get; private set; }

        [ImportingConstructor]
        public UserViewModel(ILoginService loginService, IEventAggregator eventAggregator)
        {
            LoginService = loginService;
            EventAggregator = eventAggregator;
            addCommand = new DelegateCommand<string>(Add);
            refershCommand = new DelegateCommand<string>(Refersh);
            removeCommand = new DelegateCommand<int?>(Remove);
            editCommand = new DelegateCommand<int?>(Edit);
            this.accounts = new ObservableCollection<Account>();

            this.EditUserRequest = new InteractionRequest<EditUserNotification>();


            loginService.Updated += LoginService_Updated;

        }

        private void Edit(int? id)
        {
            var account = Accounts.FirstOrDefault(x => x.Id == id);
            if (account != null)
                this.EditUserRequest.Raise(new EditUserNotification(account) { Title = "Account Edit" }, c =>
                {
                    if (c.Confirmed)
                    {
                        LoginService.Save(account);
                    }
                });
        }

        private void LoginService_Updated(object sender, AccountEventArgs e)
        {
            if (e.Account != null && (e.Account.Role | Role.Admin) == e.Account.Role)
                this.Refersh(null);
        }

        private void Remove(int? id)
        {
            throw new NotImplementedException();
        }

        private void Refersh(string obj)
        {
            Accounts.Clear();
            CurrentAccount = null;
            var accounts = LoginService.GetAllUsers();

            if (null != accounts)
                foreach (var item in accounts)
                {
                    Accounts.Add(item);
                }
        }

        private void Add(string obj)
        {
            this.EditUserRequest.Raise(new EditUserNotification(new Account { IsActive = true }) { Title = "Account Add" }, c =>
             {
                 if (c.Confirmed)
                 {
                     LoginService.Save(c.Account);
                 }
             });
        }

        public string HeaderInfo
        {
            get
            {
                return "User Info";
            }
        }

        public ObservableCollection<Account> Accounts
        {
            get { return this.accounts; }
            set
            {
                SetProperty(ref this.accounts, value);
            }
        }
        public Account CurrentAccount { get; set; }

        public ICommand AddCommand { get { return this.addCommand; } }
        public ICommand RefershCommand { get { return this.refershCommand; } }

        public ICommand RemoveCommand { get { return this.removeCommand; } }
        public ICommand EditCommand { get { return this.editCommand; } }
    }
}
