﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Trademark.Modules.Admin.User
{
    public class EditUserViewModel : BindableBase, IInteractionRequestAware
    {
        
        private EditUserNotification notification;
        public Action FinishInteraction { get; set; }

        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                SetProperty(ref this.notification, value as EditUserNotification);
            }
        }

        public EditUserViewModel()
        {
            this.SaveCommand = new DelegateCommand<PasswordBox>(this.Save);
            this.CancelCommand = new DelegateCommand(this.Cancel);
        }

        private void Save(PasswordBox box)
        {
            if (this.notification != null)
            {
                this.notification.Account.Password = box.Password;
                this.notification.Confirmed = true;
            }
            this.FinishInteraction();
        }

        private void Cancel()
        {
            if (this.notification != null)
            {
                this.notification.Account = null;
                this.notification.Confirmed = false;
            }
            this.FinishInteraction();
        }

        public ICommand SaveCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }
    }
}
