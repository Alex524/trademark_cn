﻿using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.Admin.User
{
    public class EditUserNotification : Confirmation
    {

        public EditUserNotification()
        {
            this.Account = null;
        }

        public EditUserNotification(Account account)
            : this()
        {
            this.Account = account;
        }

        public Account Account { get; set; }
    }
}
