

namespace Trademark.Infrastructure
{
    public static class RegionNames
    {
        public const string MainToolBarRegion = "MainToolBarRegion";
        public const string MainRegion = "MainRegion";
        public const string OrdersRegion = "OrdersRegion";
        public const string SecondaryRegion = "SecondaryRegion";
        public const string ActionRegion = "ActionRegion";
        public const string ResearchRegion = "ResearchRegion";
        public const string CategoryRegion = "CategoryRegion";
        public const string QARegion = "QARegion";
    }
}
