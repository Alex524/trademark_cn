

using Prism.Events;

namespace Trademark.Infrastructure
{
    public class TickerSymbolSelectedEvent : PubSubEvent<string>
    {
    }
}
