﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure
{
    public enum NameType : byte
    {
        Word = 1,
        Design = 2,
        Mix = 3
    }

    [Flags]
    public enum TMStatus : byte
    {
        Load =1,
        Add = 2,
        EidtSubmit = 4, 
        QAReturn = 8,
        QAPass = 16,
        NameImported = 32,
    }
}
