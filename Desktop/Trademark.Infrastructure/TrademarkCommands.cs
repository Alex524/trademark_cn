

using Prism.Commands;
using System.ComponentModel.Composition;

namespace Trademark.Infrastructure
{

    public static class TrademarkCommands
    {
        private static CompositeCommand submitOrderCommand = new CompositeCommand(true);
        private static CompositeCommand cancelOrderCommand = new CompositeCommand(true);
        private static CompositeCommand submitAllOrdersCommand = new CompositeCommand();
        private static CompositeCommand cancelAllOrdersCommand = new CompositeCommand();

        public static CompositeCommand SubmitOrderCommand
        {
            get { return submitOrderCommand; }
            set { submitOrderCommand = value; }
        }

        public static CompositeCommand CancelOrderCommand
        {
            get { return cancelOrderCommand; }
            set { cancelOrderCommand = value; }
        }

        public static CompositeCommand SubmitAllOrdersCommand
        {
            get { return submitAllOrdersCommand; }
            set { submitAllOrdersCommand = value; }
        }

        public static CompositeCommand CancelAllOrdersCommand
        {
            get { return cancelAllOrdersCommand; }
            set { cancelAllOrdersCommand = value; }
        }
    }

    [Export]    
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class TrademarkCommandProxy
    {
        virtual public CompositeCommand SubmitOrderCommand
        {
            get { return TrademarkCommands.SubmitOrderCommand; }
        }

        virtual public CompositeCommand CancelOrderCommand
        {
            get { return TrademarkCommands.CancelOrderCommand; }
        }

        virtual public CompositeCommand SubmitAllOrdersCommand
        {
            get { return TrademarkCommands.SubmitAllOrdersCommand; }
        }

        virtual public CompositeCommand CancelAllOrdersCommand
        {
            get { return TrademarkCommands.CancelAllOrdersCommand; }
        }
    }
}
