


namespace Trademark.Infrastructure
{
    public interface IViewRegionRegistration
    {
        string RegionName { get; }
    }
}
