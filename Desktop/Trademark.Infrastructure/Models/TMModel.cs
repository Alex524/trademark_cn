﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Infrastructure.Models
{
    public class TMItem : BindableBase
    {
        private int id;
        private DateTime? ggrq;
        private int? ggqh;
        private int? pageNo;
        private string pubNumber;
        private DateTime? applicantDate;
        private string nameCN;
        private string nameEN;
        private byte[] tmImage;
        private string applicantCN;
        private string applicantEN;
        private string addressCN;
        private string addressEN;
        private string agentCN;
        private string agentEN;
        private TMStatus? status;
        private int? userId;
        private DateTime? lastUpdate;
        private NameType? nameType;
        private string rejectReason;
        private string remark;
        private string displayName;
        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                SetProperty(ref this.id, value);
            }
        }

        public DateTime? GGRQ
        {
            get
            {
                return ggrq;
            }

            set
            {
                SetProperty(ref this.ggrq, value);
            }
        }

        public int? GGQH
        {
            get
            {
                return ggqh;
            }

            set
            {
                SetProperty(ref this.ggqh, value);
            }
        }

        public int? PageNo
        {
            get
            {
                return pageNo;
            }

            set
            {
                SetProperty(ref this.pageNo, value);
            }
        }

        public string PubNumber
        {
            get
            {
                return pubNumber;
            }

            set
            {
                SetProperty(ref this.pubNumber, value);
            }
        }

        public DateTime? ApplicantDate
        {
            get
            {
                return applicantDate;
            }

            set
            {
                SetProperty(ref this.applicantDate, value);
            }
        }

        public string NameCN
        {
            get
            {
                return nameCN;
            }

            set
            {
                SetProperty(ref this.nameCN, value);
            }
        }

        public string NameEN
        {
            get
            {
                return nameEN;
            }

            set
            {
                SetProperty(ref this.nameEN, value);
            }
        }

        public byte[] TMImage
        {
            get
            {
                return tmImage;
            }

            set
            {
                SetProperty(ref this.tmImage, value);
            }
        }

        public string ApplicantCN
        {
            get
            {
                return applicantCN;
            }

            set
            {
                SetProperty(ref this.applicantCN, value);
            }
        }

        public string ApplicantEN
        {
            get
            {
                return applicantEN;
            }

            set
            {
                SetProperty(ref this.applicantEN, value);
            }
        }

        public string AddressCN
        {
            get
            {
                return addressCN;
            }

            set
            {
                SetProperty(ref this.addressCN, value);
            }
        }

        public string AddressEN
        {
            get
            {
                return addressEN;
            }

            set
            {
                SetProperty(ref this.addressEN, value);
            }
        }

        public string AgentCN
        {
            get
            {
                return agentCN;
            }

            set
            {
                SetProperty(ref this.agentCN, value);
            }
        }

        public string AgentEN
        {
            get
            {
                return agentEN;
            }

            set
            {
                SetProperty(ref this.agentEN, value);
            }
        }

        public TMStatus? Status
        {
            get
            {
                return status;
            }

            set
            {
                SetProperty(ref this.status, value);
            }
        }

        public int? UserId
        {
            get
            {
                return userId;
            }

            set
            {
                SetProperty(ref this.userId, value);
            }
        }

        public DateTime? LastUpdate
        {
            get
            {
                return lastUpdate;
            }

            set
            {
                SetProperty(ref this.lastUpdate, value);
            }
        }

        public string RejectReason
        {
            get
            {
                return rejectReason;
            }

            set
            {
                SetProperty(ref this.rejectReason, value);
            }
        }

        public NameType? NameType { get { return nameType; } set { this.SetProperty(ref nameType, value); } }

        public ObservableCollection<TMCategory> Categories { get; set; }

        public string DisplayName
        {
            get
            {
                return displayName;
            }

            set
            {
                SetProperty(ref displayName, value);
            }
        }

        public string Remark
        {
            get
            {
                return remark;
            }

            set
            {
                SetProperty(ref this.remark, value);
            }
        }

        public TMItem()
        {
            this.Categories = new ObservableCollection<TMCategory>();
        }

        public TMItem(TMElement item) : this()
        {
            this.Copy(item);
        }

        public void Copy(TMElement source)
        {
            this.Id = source.Id;
            this.GGQH = source.GGQH;
            this.GGRQ = source.GGRQ;
            this.PageNo = source.PageNo;
            this.PubNumber = source.PubNumber;
            this.ApplicantDate = source.ApplicantDate;
            this.NameCN = source.NameCN;
            this.NameEN = source.NameEN;
            this.NameType = (NameType?)source.NameType;
            this.TMImage = source.TMImage;
            this.ApplicantCN = source.ApplicantCN;
            this.ApplicantEN = source.ApplicantEN;
            this.AddressCN = source.AddressCN;
            this.AddressEN = source.AddressEN;
            this.AgentCN = source.AgentCN;
            this.AgentEN = source.AgentEN;
            this.Status = (TMStatus?)source.Status;
            this.UserId = source.UserId;
            this.LastUpdate = source.LastUpdate;
            this.RejectReason = source.RejectReason;
            this.Remark = source.Remark;
            this.DisplayName = source.DisplayName;

            this.Categories.Clear();

            if (null != source.Categories)
                foreach (var category in source.Categories)
                {
                    this.Categories.Add(new TMCategory(category));
                }
        }

        public void CopyWithoutId(TMElement source)
        {
            this.GGQH = source.GGQH;
            this.GGRQ = source.GGRQ;
            this.PageNo = source.PageNo;
            this.PubNumber = source.PubNumber;
            this.ApplicantDate = source.ApplicantDate;
            this.NameCN = source.NameCN;
            this.NameEN = source.NameEN;
            this.NameType = (NameType?)source.NameType;
            this.TMImage = source.TMImage;
            this.ApplicantCN = source.ApplicantCN;
            this.ApplicantEN = source.ApplicantEN;
            this.AddressCN = source.AddressCN;
            this.AddressEN = source.AddressEN;
            this.AgentCN = source.AgentCN;
            this.AgentEN = source.AgentEN;
            //this.Status = (TMStatus?)source.Status;
            //this.UserId = source.UserId;
            this.LastUpdate = source.LastUpdate;
            this.RejectReason = source.RejectReason;
            this.Remark = source.Remark;
            this.DisplayName = source.DisplayName;

            this.Categories.Clear();
            if (null != source.Categories)
                foreach (var category in source.Categories)
                {
                    category.Id = 0;
                    this.Categories.Add(new TMCategory(category));
                }

        }
    }

    public class TMCategory : BindableBase
    {
        private int id;
        private int? tm_Id;
        private int? category;
        private string description;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                SetProperty(ref this.id, value);
            }
        }

        public int? TM_Id
        {
            get
            {
                return tm_Id;
            }

            set
            {
                SetProperty(ref this.tm_Id, value);
            }
        }

        public int? Category
        {
            get
            {
                return category;
            }

            set
            {
                SetProperty(ref this.category, value);
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                SetProperty(ref this.description, value);
            }
        }

        public TMCategory()
        {

        }

        public TMCategory(Category category) : this()
        {
            this.Copy(category);
        }

        public void Copy(Category source)
        {
            this.Id = source.Id;
            this.TM_Id = source.TMElement_Id;
            this.Category = source.CategoryNumber;
            this.Description = source.Description;
        }
    }

    public class TMCollection : ObservableCollection<TMItem>
    {
        public TMCollection() : base()
        {
        }

        public TMCollection(IEnumerable<TMElement> list) :
            this(list.Select(x => new TMItem(x)))
        {
        }

        public TMCollection(IEnumerable<TMItem> list) : base()
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }

            this.Items.Clear();

            foreach (TMItem item in list)
            {
                this.Add(item);
            }
        }
    }
}
