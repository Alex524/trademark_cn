﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure.Models
{
    public class Account : BindableBase
    {
        private int id;
        private string name;
        private string displayName;
        private string password;
        private Role? role;
        private bool? isActive;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                SetProperty(ref id, value);
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                SetProperty(ref name, value);
            }
        }

        public string DisplayName
        {
            get
            {
                return displayName;
            }

            set
            {
                SetProperty(ref displayName, value);
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                SetProperty(ref password, value);
            }
        }

        public Role? Role
        {
            get
            {
                return role;
            }

            set
            {
                SetProperty(ref role, value);
            }
        }

        public bool? IsActive
        {
            get
            {
                return isActive;
            }

            set
            {
                SetProperty(ref isActive, value);
            }
        }
    }

    public class CurrentPage
    {
        public int PageNo { get; set; }
        public DateTime? GGRQ { get; set; }
        public int? GGQH { get; set; }
        public int? UserId { get; set; }
    }

    [Flags]
    public enum Role : int
    {
        Edit = 1, QA = 2, NameTR = 4, NameQA = 8, Admin = 16
    }
}
