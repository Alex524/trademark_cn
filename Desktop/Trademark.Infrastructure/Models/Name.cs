﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure.Models
{
    public class Name : BindableBase
    {
        private int _ID;
        private int? _TMQH;
        private int? _Type;
        private string _CN;
        private string _EN;
        private int? _TMID;
        private int? _Category;
        private int? _Status;
        private int? _TRUserID;
        private int? _QAUserID;
        private DateTime? _TRDate;
        private DateTime? _QADate;
        private string reason;
        public int ID
        {
            get
            {
                return _ID;
            }

            set
            {
                SetProperty(ref _ID, value);
            }
        }

        public int? TMQH
        {
            get
            {
                return _TMQH;
            }

            set
            {
                SetProperty(ref _TMQH, value);
            }
        }

        public int? Type
        {
            get
            {
                return _Type;
            }

            set
            {
                SetProperty(ref _Type, value);
            }
        }

        public string CN
        {
            get
            {
                return _CN;
            }

            set
            {
                SetProperty(ref _CN, value);
            }
        }

        public string EN
        {
            get
            {
                return _EN;
            }

            set
            {
                SetProperty(ref _EN, value);
            }
        }

        public int? TMID
        {
            get
            {
                return _TMID;
            }

            set
            {
                SetProperty(ref _TMID, value);
            }
        }

        public int? Category
        {
            get
            {
                return _Category;
            }

            set
            {
                SetProperty(ref _Category, value);
            }
        }

        public int? Status
        {
            get
            {
                return _Status;
            }

            set
            {
                SetProperty(ref _Status, value);
            }
        }

        public int? TRUserID
        {
            get
            {
                return _TRUserID;
            }

            set
            {
                SetProperty(ref _TRUserID, value);
            }
        }

        public int? QAUserID
        {
            get
            {
                return _QAUserID;
            }

            set
            {
                SetProperty(ref _QAUserID, value);
            }
        }

        public DateTime? TRDate
        {
            get
            {
                return _TRDate;
            }

            set
            {
                SetProperty(ref _TRDate, value);
            }
        }

        public DateTime? QADate
        {
            get
            {
                return _QADate;
            }

            set
            {
                SetProperty(ref _QADate, value);
            }
        }

        public string Reason
        {
            get
            {
                return reason;
            }

            set
            {
                SetProperty(ref reason, value);
            }
        }
    }
}
