﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure.Models
{
    public class AccountEventArgs : EventArgs
    {
        public AccountEventArgs(Account account)
        {
            this.Account = account;
        }

        public Account Account { get; set; }
    }
}
