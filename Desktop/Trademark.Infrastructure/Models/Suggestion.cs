﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure.Models
{
    public class Suggestion
    {
        public ConfigureType Type { get; set; }
        public string CN { get; set; }
        public string EN { get; set; }
    }
}
