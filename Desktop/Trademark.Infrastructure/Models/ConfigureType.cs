﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure.Models
{
    [Flags]
    public enum ConfigureType : int
    {
        Name = 1,
        Applicant = 2,
        Agent = 4,
        Address = 8,
        Category = 16
    }
}
