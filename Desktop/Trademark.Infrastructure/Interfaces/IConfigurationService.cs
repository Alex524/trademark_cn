﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface IConfigurationService
    {
        void Download(ConfigureType type);
        void Set(Stream file);
        IEnumerable Get(ConfigureType type, string filter);
    }
}
