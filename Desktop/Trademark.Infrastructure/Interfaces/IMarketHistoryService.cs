

using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface IMarketHistoryService
    {
        MarketHistoryCollection GetPriceHistory(string tickerSymbol);
    }
}
