﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;
using Trademark.Service.Contract;

namespace Trademark.Infrastructure.Interfaces
{
    public interface ITrademarkFeedService
    {
        TMElement GetProcessingPage(int userId);
        IList<TMElement> Search(SearchModel search);

        SearchModel GetCurrentPage();

        void SetCurrentPage(SearchModel model);
        TMElement Find(string pubNumber);

        TMElement Save(TMElement tm);

        void Remove(int? id);

    }
}
