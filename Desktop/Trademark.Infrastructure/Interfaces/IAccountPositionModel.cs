

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface IAccountPositionService
    {
        event EventHandler<AccountPositionModelEventArgs> Updated;
        IList<AccountPosition> GetAccountPositions();
    }
}
