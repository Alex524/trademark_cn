﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface INameFeedService
    {
        Name GetTRName(int? pubNo, int? type, int? category, int? userId);
        Name GetQAName(int? pubNo, int? type, int? category, int? userId);

        void Submit(Name name);
        void Reject(Name name);
        void QAReject(Name name);

    }

    public interface INameReferenceService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TM.ID</param>
        /// <returns></returns>
        TMItem GetReference(int id);
    }
}
