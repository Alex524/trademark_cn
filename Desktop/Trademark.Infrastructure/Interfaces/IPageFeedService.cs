﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Infrastructure.Interfaces
{
    public interface IPageFeedService
    {
        TMPage GetCurrentPage();

        void SetCurrentPage(TMPage model);

        TMPage GetPage(bool refresh);

        TMPage GetSpecialPage(int? fromNo, int? toNo);

        void UpdatePage(byte Status);
    }
}
