﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface ILoginService
    {
        void Login(string name, string password);
        Account GetAccount();

        IList<Account> GetAllUsers();

        event EventHandler<AccountEventArgs> Updated;

        void Save(Account account);

    }
}
