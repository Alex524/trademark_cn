

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure.Interfaces
{
    public interface INewsFeedService
    {
        IList<NewsArticle> GetNews(string tickerSymbol);
        bool HasNews(string tickerSymbol);
        event EventHandler<NewsFeedEventArgs> Updated;

        
    }
}
