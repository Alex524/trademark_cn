﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Infrastructure
{
    public class ESBClient : IDisposable
    {
        public void Dispose()
        {
            if (null != this.ChannelFactory)
                this.ChannelFactory.Close();
        }

        public T GetClientProxy<T>()
        {
            Type contractType = typeof(T);
            string contractName = contractType.FullName;
            this.ChannelFactory = new ChannelFactory<T>(contractName);

            AttachContextBehavior behavior = new AttachContextBehavior();


            this.ChannelFactory.Endpoint.Behaviors.Add(behavior);

            return ((ChannelFactory<T>)this.ChannelFactory).CreateChannel();
        }

        //private static System.ServiceModel.ChannelFactory CreateChannelFactory(Type contractType, string contractName)
        //{
        //    //System.ServiceModel.ChannelFactory channelFactory = (System.ServiceModel.ChannelFactory)Activator.CreateInstance(contractType, new object[] { contractName });
        //    System.ServiceModel.ChannelFactory channelFactory = (System.ServiceModel.ChannelFactory)Activator.CreateInstance(contractType, contractName);
        //    //if (channelFactoryParam.IsAttachUserToken)
        //    //{
        //    //    AttachContextBehavior behavior = new AttachContextBehavior
        //    //    {
        //    //        CurrentUser = currentUser
        //    //    };
        //    //    channelFactory.Endpoint.Behaviors.Add(behavior);
        //    //}
        //    return channelFactory;
        //}

        public System.ServiceModel.ChannelFactory ChannelFactory { get; private set; }
    }

    public class AttachContextBehavior : BehaviorExtensionElement, IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        protected override object CreateBehavior()
        {
            return new AttachContextBehavior();
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public override System.Type BehaviorType
        {
            get
            {
                return typeof(AttachContextBehavior);
            }
        }

        //public User CurrentUser { get; set; }
    }

    public class ServiceWrapper<T> : IDisposable where T : class
    {
        ChannelFactory<T> factory;
        private T channel;

        private readonly BasicHttpBinding binding;
        private readonly EndpointAddress endpoint;

        private readonly object lockObject = new object();
        private bool disposed;

        //public ServiceWrapper(string configName)
        //{
        //    if (ConfigurationManager.AppSettings[configName] == null)
        //    {
        //        throw new ConfigurationErrorsException(configName + " is not present in the config file");
        //    }

        //    binding = new BasicHttpBinding();
        //    endpoint = new EndpointAddress(ConfigurationManager.AppSettings[configName]);
        //    disposed = false;
        //}

        public T Channel
        {
            get
            {
                if (disposed)
                {
                    throw new ObjectDisposedException("Resource ServiceWrapper<" + typeof(T) + "> has been disposed");
                }

                lock (lockObject)
                {
                    if (factory == null)
                    {
                        factory = new ChannelFactory<T>(typeof(T).Name);
                        channel = factory.CreateChannel();
                    }
                }
                return channel;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    lock (lockObject)
                    {
                        if (channel != null)
                        {
                            ((IClientChannel)channel).Close();
                        }
                        if (factory != null)
                        {
                            factory.Close();
                        }
                    }

                    channel = null;
                    factory = null;
                    disposed = true;
                }
            }
        }
    }

}
