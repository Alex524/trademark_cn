﻿using Prism.Events;

namespace Trademark.Infrastructure
{
    public class ShowLoginEvent : PubSubEvent<string>
    {
    }
}
