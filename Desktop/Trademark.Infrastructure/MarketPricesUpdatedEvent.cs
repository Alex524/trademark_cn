

using System.Collections.Generic;
using Prism.Events;

namespace Trademark.Infrastructure
{
    public class MarketPricesUpdatedEvent : PubSubEvent<IDictionary<string, decimal>>
    {
    }
}
