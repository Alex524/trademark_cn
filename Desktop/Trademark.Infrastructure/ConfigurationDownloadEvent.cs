﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Infrastructure
{
    public class ConfigurationDownloadEvent : PubSubEvent<ConfigureType>
    {
    }
}
