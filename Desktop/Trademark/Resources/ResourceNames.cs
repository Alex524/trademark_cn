

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trademark.Resources
{
    internal static class ResourceNames
    {
        public const string EntryStoryboardName = "InTransition";
        public const string ExitStoryboardName = "OutTransition";
    }
}
