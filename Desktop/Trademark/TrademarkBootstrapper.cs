

using System;
using System.ComponentModel.Composition.Hosting;
using System.Windows;
using Trademark.Infrastructure;
using Prism.Mef;
using Trademark.Modules.Login;
using System.ComponentModel.Composition;
using Prism.Events;
using Microsoft.Practices.ServiceLocation;
using Trademark.Modules.Edit;
using Trademark.Modules.EditDetails;
using Trademark.Modules.Name;
using Trademark.Modules.Admin;

namespace Trademark
{
    [CLSCompliant(false)]
    public partial class TrademarkBootstrapper : MefBootstrapper
    {
        [Import]
        private IEventAggregator EventAggregator;
        protected override void ConfigureAggregateCatalog()
        {
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(TrademarkBootstrapper).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(LoginModule).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(TrademarkCommands).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(EditModule).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(EditDetailsModule).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(NameModule).Assembly));
            this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(AdminModule).Assembly));
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();

            Application.Current.MainWindow = (Shell)this.Shell;
            Application.Current.MainWindow.Show();
        }


        protected override Prism.Regions.IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
        {
            var factory = base.ConfigureDefaultRegionBehaviors();

            factory.AddIfMissing("AutoPopulateExportedViewsBehavior", typeof(AutoPopulateExportedViewsBehavior));

            return factory;
        }

        protected override DependencyObject CreateShell()
        {
            return this.Container.GetExportedValue<Shell>();
        }

        protected override void InitializeModules()
        {
            base.InitializeModules();
            ServiceLocator.Current.GetInstance<IEventAggregator>().GetEvent<ShowLoginEvent>().Publish(null);
        }
    }
}
