

using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Modules.Name;

namespace Trademark
{
    [Export]
    public class ShellViewModel : BindableBase
    {
        // This is where any view model logic for the shell would go.
        private string name;
        private string displayName;
        private readonly IRegionManager regionManager;
        private DelegateCommand<string> changeUserCommand { get; set; }

        private IEventAggregator EventAggregator { get; set; }

        [ImportingConstructor]
        public ShellViewModel(ILoginService loginService, IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            loginService.Updated += LoginService_Updated;
            this.EventAggregator = eventAggregator;
            this.regionManager = regionManager;
            this.changeUserCommand = new DelegateCommand<string>(this.ChangeUser);
        }

        private void ChangeUser(string name)
        {
            this.EventAggregator.GetEvent<ShowLoginEvent>().Publish(this.Name);
        }

        private void LoginService_Updated(object sender, Infrastructure.Models.AccountEventArgs e)
        {
            if (e.Account != null)
            {
                this.Name = e.Account.Name;
                this.DisplayName = e.Account.DisplayName;
                this.CloseLoginForm();

                foreach (var view in this.regionManager.Regions[RegionNames.MainRegion].Views)
                {
                    var hasrole = false;
                    if (view.GetType() == typeof(Trademark.Modules.Edit.Summary.EditSummaryView) && (e.Account.Role.Value | Role.Edit) == e.Account.Role.Value)
                        hasrole = true;
                    if (view.GetType() == typeof(Trademark.Modules.Edit.QA.QASummaryView) && (e.Account.Role.Value | Role.QA) == e.Account.Role.Value)
                        hasrole = true;
                    if (view.GetType() == typeof(Trademark.Modules.Name.TR.NameTRView) && (e.Account.Role.Value | Role.NameTR) == e.Account.Role.Value)
                        hasrole = true;
                    if (view.GetType() == typeof(Trademark.Modules.Name.QA.NameQAView) && (e.Account.Role.Value | Role.NameQA) == e.Account.Role.Value)
                        hasrole = true;

                    if ((e.Account.Role.Value | Role.Admin) == e.Account.Role.Value)
                        hasrole = true;
                    if (!hasrole)
                        this.regionManager.Regions[RegionNames.MainRegion].Remove(view);
                }
                //ServiceLocator.Current.GetInstance<ComposablePartCatalog>().Remove(new AssemblyCatalog(typeof(NameModule).Assembly));

            }
            else
            {
                this.Name = null;
                this.DisplayName = null;
            }


        }

        private void CloseLoginForm()
        {
            foreach (var view in this.regionManager.Regions[RegionNames.SecondaryRegion].Views)
            {
                this.regionManager.Regions[RegionNames.SecondaryRegion].Remove(view);
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                SetProperty(ref this.name, value);
            }
        }

        public string DisplayName
        {
            get { return displayName; }
            set
            {
                SetProperty(ref this.displayName, value);
            }
        }

        public ICommand ChangeUserCommand { get { return this.changeUserCommand; } }


    }
}
