﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.EditDetails.Config
{
    [Export(typeof(IConfigFileDownloadViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ConfigFileDownloadViewModel : BindableBase, IConfigFileDownloadViewModel
    {
        private ICommand downloadCommand;
        private readonly IEventAggregator EventAggregator;
        private readonly IConfigurationService ConfigurationService;

        [ImportingConstructor]
        public ConfigFileDownloadViewModel(IEventAggregator eventAggregator, IConfigurationService configurationService)
        {
            this.EventAggregator = eventAggregator;
            this.ConfigurationService = configurationService;
            downloadCommand = new DelegateCommand<string>(Download);
        }

        private void Download(string obj)
        {
            this.ConfigurationService.Download(ConfigureType.Address | ConfigureType.Name | ConfigureType.Applicant | ConfigureType.Agent | ConfigureType.Category);

            this.EventAggregator.GetEvent<ConfigurationDownloadEvent>().Publish(ConfigureType.Address | ConfigureType.Name | ConfigureType.Applicant | ConfigureType.Agent | ConfigureType.Category);
        }

        public string HeaderInfo
        {
            get
            {
                return "Configuation";
            }
        }

        public ICommand DownloadCommand { get { return this.downloadCommand; } }
    }
}
