﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trademark.Modules.EditDetails.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Trademark.Modules.EditDetails.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;data&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;（株）LS化妆品公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;（株）芝爱堂&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;《中国书画》杂志社&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;《中国学校体育》杂志有限责任公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;BSN医疗有限公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;BSN医药公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;CJ第一制糖株式会社&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;CS保健氏股份有限公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;HQG有限公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;K11集团有限公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;LRC制品有限公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;MGA娱乐公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;PLR智慧财产控股有限责任公司&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;R＆B不动产集团（加利福尼亚州有限合伙企业）&quot; /&gt;
        ///&lt;row Type=&quot;2&quot; CN=&quot;Ror [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Configuration {
            get {
                return ResourceManager.GetString("Configuration", resourceCulture);
            }
        }
    }
}
