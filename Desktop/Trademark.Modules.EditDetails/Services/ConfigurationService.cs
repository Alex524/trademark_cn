﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Modules.EditDetails.Properties;

namespace Trademark.Modules.EditDetails.Services
{
    [Export(typeof(IConfigurationService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class ConfigurationService : IConfigurationService
    {
        private Dictionary<ConfigureType, IEnumerable<Suggestion>> _suggestions;

        public ConfigurationService()
        {

            var doc = XDocument.Parse(Resources.Configuration);
            this._suggestions = Config(doc);
        }

        private Dictionary<ConfigureType, IEnumerable<Suggestion>> Config(XDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException("document");
            }

            return document.Descendants("row")
                .GroupBy(x => (ConfigureType)int.Parse(x.Attribute("Type").Value),
                         x => new Suggestion
                         {
                             CN = x.Attribute("CN").Value,
                             EN = x.Attribute("EN") != null ? x.Attribute("EN").Value : null
                         })
                .ToDictionary(group => group.Key, group => group.Distinct(new coparer()));
        }

        public void Download(ConfigureType type)
        {
            throw new NotImplementedException();
        }

        public IEnumerable Get(ConfigureType type, string filter)
        {
            if (this._suggestions.ContainsKey(type) && this._suggestions[type] != null)
                return this._suggestions[type].Where(x => x.CN.Contains(filter));

            return null;
        }

        public void Set(Stream file)
        {
            throw new NotImplementedException();
        }
    }

    public class coparer : IEqualityComparer<Suggestion>
    {
        public bool Equals(Suggestion x, Suggestion y)
        {
            return x.CN.Equals(y.CN);
        }

        public int GetHashCode(Suggestion obj)
        {
            return obj.GetHashCode();
        }
    }

}
