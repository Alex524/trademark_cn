﻿using System.Linq;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.EditDetails.Services
{
    internal class SuggestionHashSet
    {
        private IGrouping<string, Suggestion> group;

        public SuggestionHashSet(IGrouping<string, Suggestion> group)
        {
            this.group = group;
        }
    }
}