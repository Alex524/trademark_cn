﻿using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.EditDetails
{
    public interface IConfigFileDownloadViewModel : IHeaderInfoProvider<string>
    {
        
    }
}