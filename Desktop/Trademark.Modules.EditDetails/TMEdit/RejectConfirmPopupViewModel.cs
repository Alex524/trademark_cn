﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Trademark.Modules.EditDetails.TMEdit
{
    public class RejectConfirmPopupViewModel : BindableBase, IInteractionRequestAware
    {
        private Confirmation confirmation;
        private ICommand okCommand;
        private ICommand cancelCommand;

        public Action FinishInteraction { get; set; }

        public INotification Notification
        {
            get
            {
                return this.confirmation;
            }

            set
            {
                SetProperty(ref this.confirmation, value as Confirmation);
            }
        }

        public RejectConfirmPopupViewModel()
        {
            this.okCommand = new DelegateCommand<string>(Ok);
            this.cancelCommand = new DelegateCommand<string>(Cancel);
        }

        private void Cancel(string obj)
        {
            if (this.confirmation != null)
                this.confirmation.Confirmed = false;

            this.FinishInteraction();
        }

        private void Ok(string obj)
        {
            if (this.confirmation != null)
            {
                this.confirmation.Confirmed = true;
            }
            this.FinishInteraction();
        }

        public ICommand OkCommand { get { return this.okCommand; } }
        public ICommand CancelCommand { get { return this.cancelCommand; } }
    }
}
