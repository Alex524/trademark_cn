﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;

namespace Trademark.Modules.EditDetails.TMEdit
{
    [Export(typeof(EditCompositeViewModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditCompositeViewModel
    {
        private readonly EditTrademarkViewModel editTrademarkViewModel;
        private readonly CategoryViewModel categoryViewModel;
        private readonly IEventAggregator EventAggregator;

        [ImportingConstructor]
        public EditCompositeViewModel(EditTrademarkViewModel editTrademarkModel, CategoryViewModel categoryViewModel, IEventAggregator eventAggregator)
        {
            this.editTrademarkViewModel = editTrademarkModel;
            this.categoryViewModel = categoryViewModel;
            this.categoryViewModel.SetTMModel(editTrademarkViewModel);
            this.EventAggregator = eventAggregator;
            this.EventAggregator.GetEvent<TMSelectedEvent>().Subscribe(editTrademarkModel.TMSelectChanged);
            this.editTrademarkViewModel.PropertyChanged += EditTrademarkViewModel_PropertyChanged;
        }

        private void EditTrademarkViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TMItem" && null != this.TrademarkModel.TMItem)
                this.CategoryModel.Categories = this.TrademarkModel.TMItem.Categories;
        }

        public EditTrademarkViewModel TrademarkModel { get { return this.editTrademarkViewModel; } }
        public CategoryViewModel CategoryModel { get { return this.categoryViewModel; } }
    }
}
