﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;

namespace Trademark.Modules.EditDetails.TMEdit
{
    /// <summary>
    /// Interaction logic for EditCompositeView.xaml
    /// </summary>
    [ViewExport(RegionName = RegionNames.ResearchRegion)]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditCompositeView : UserControl
    {
        public EditCompositeView()
        {
            InitializeComponent();
        }

        [Import]
        EditCompositeViewModel model
        { set { this.DataContext = value; } }


    }
}
