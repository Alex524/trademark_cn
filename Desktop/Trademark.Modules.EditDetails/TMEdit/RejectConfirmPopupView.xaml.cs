﻿using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trademark.Modules.EditDetails.TMEdit
{
    /// <summary>
    /// Interaction logic for RejectConfirmPopupView.xaml
    /// </summary>
    public partial class RejectConfirmPopupView : UserControl
    {
        private Confirmation confirmation;
        public RejectConfirmPopupView()
        {
            InitializeComponent();
            this.DataContext = new RejectConfirmPopupViewModel();
        }
    }
}
