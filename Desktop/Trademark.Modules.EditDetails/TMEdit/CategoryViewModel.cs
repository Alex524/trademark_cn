﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using WpfControls.Editors;
using System.Collections;

namespace Trademark.Modules.EditDetails.TMEdit
{
    [Export(typeof(CategoryViewModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CategoryViewModel : BindableBase
    {
        private TMCategory selectedCategory;
        private ObservableCollection<TMCategory> categories;
        private readonly ICommand showCategoryListCommand;
        private readonly ICommand addCategoryCommand;
        private readonly ICommand removeCategoryCommand;

        /// <param name="trademarkViewModel">improt here to use the trademark view model's TMItem.Categories to add or update the current category while saving the edit item.</param>
        [ImportingConstructor]
        public CategoryViewModel(IConfigurationService configurationService)
        {
            this.ConfigurationService = configurationService;
            this.showCategoryListCommand = new DelegateCommand<string>(ShowList);
            this.addCategoryCommand = new DelegateCommand<string>(AddCategory);
            this.removeCategoryCommand = new DelegateCommand<int?>(Remove);
            this.addtoDescCommand = new DelegateCommand<string>(AddtoDesc);
            this.categoryProvider = new SuggestionProvider(CategorySearch);

        }

        private IEnumerable CategorySearch(string arg)
        {
            return this.ConfigurationService.Get(ConfigureType.Category, arg);
        }

        private void AddtoDesc(string obj)
        {
            SelectedCategory.Description += "；" + this.InputCategory;

            this.InputCategory = null;
        }

        private void Remove(int? id)
        {
            var removeitem = this.Categories.FirstOrDefault(x => x.Id == id);
            if (null != removeitem)
                this.Categories.Remove(removeitem);
        }

        private void AddCategory(string obj)
        {
            this.SelectedCategory = new TMCategory();
            this.categories.Add(this.selectedCategory);
        }

        private void ShowList(string obj)
        {
            this.TrademarkModel.Save(null);
            this.SelectedCategory = null;
        }

        public void SetTMModel(EditTrademarkViewModel item)
        {
            this.TrademarkModel = item;
        }
        

        public TMCategory SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                SetProperty(ref this.selectedCategory, value);
            }
        }

        private string inputCategory;
        private readonly ICommand addtoDescCommand;

        public ObservableCollection<TMCategory> Categories { get { return this.categories; } set { SetProperty(ref this.categories, value); } }

        public ICommand ShowCategoryListCommand { get { return this.showCategoryListCommand; } }

        public ICommand AddCategoryCommand { get { return this.addCategoryCommand; } }

        public ICommand RemoveCategoryCommand { get { return this.removeCategoryCommand; } }

        public ICommand AddtoDescCommand { get { return this.addtoDescCommand; } }

        public string InputCategory
        {
            get
            {
                return inputCategory;
            }

            set
            {
                SetProperty(ref this.inputCategory, value);
            }
        }

        private Suggestion selectedSuggestion;

        public IConfigurationService ConfigurationService { get; private set; }

        private ISuggestionProvider categoryProvider;
        public ISuggestionProvider CategoryProvider { get { return this.categoryProvider; } }

        public Suggestion SelectedSuggestion
        {
            get
            {
                return selectedSuggestion;
            }

            set
            {
                SetProperty(ref this.selectedSuggestion, value);
            }
        }

        public EditTrademarkViewModel TrademarkModel { get; private set; }
    }
}
