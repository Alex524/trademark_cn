﻿using CaptureLib;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using LH.Helper;
using Trademark.Service.Contract;
using WpfControls.Editors;
using Prism.Interactivity.InteractionRequest;

namespace Trademark.Modules.EditDetails.TMEdit
{
    [Export(typeof(EditTrademarkViewModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditTrademarkViewModel : BindableBase
    {
        private TMItem tmItem;
        private readonly ILoginService LoginService;
        private readonly ITrademarkFeedService TrademarkFeedService;
        private readonly IEventAggregator EventAggregator;
        private readonly ICommand findTrademarkCommand;
        private readonly ICommand cropCommand;
        private readonly ScreenCaputre Caputre;
        private ICommand saveCommand;
        private ICommand cleanCropCommand;

        [ImportingConstructor]
        public EditTrademarkViewModel(ILoginService loginService,
            ITrademarkFeedService trademarkFeedService,
            IConfigurationService configurationService,
            IEventAggregator eventAggregator)
        {
            this.ConfigurationService = configurationService;
            this.LoginService = loginService;
            this.TrademarkFeedService = trademarkFeedService;
            this.EventAggregator = eventAggregator;

            this.cropCommand = new DelegateCommand<object>(Crop);
            this.cleanCropCommand = new DelegateCommand<object>(CleanCrop);
            this.findTrademarkCommand = new DelegateCommand<string>(FindTrademark);
            this.saveCommand = new DelegateCommand<TMStatus?>(Save, CanSave);
            Caputre = new ScreenCaputre();
            Caputre.ScreenCaputred += Caputre_ScreenCaputred;

            ConfirmationRequest = new InteractionRequest<Confirmation>();
            RejectRequest = new InteractionRequest<Confirmation>();
        }

        private bool CanSave(TMStatus? arg)
        {
            return tmItem != null && (arg == TMStatus.EidtSubmit || TMItem.Status < arg);// && (TMItem.Status | arg) != tmItem.Status;
        }

        public InteractionRequest<Confirmation> ConfirmationRequest { get; private set; }
        public InteractionRequest<Confirmation> RejectRequest { get; private set; }
        private void CleanCrop(object obj)
        {
            TMItem.TMImage = null;
        }



        public void Save(TMStatus? status)
        {
            bool flag = true;
            string rejectReason = null;
            if (status == TMStatus.QAReturn)
            {
                this.RejectRequest.Raise(
                new Confirmation { Content = "", Title = "Confirmation" },
                c =>
                {
                    flag = c.Confirmed;
                    rejectReason = (string)c.Content;
                });
            }

            if (flag)
            {
                var sresult = GetStatus(TMItem.Status, status);

                TMElement tm = new TMElement
                {
                    Id = TMItem.Id,
                    GGQH = TMItem.GGQH,
                    GGRQ = TMItem.GGRQ,
                    PageNo = TMItem.PageNo.HasValue ? TMItem.PageNo : TrademarkFeedService.GetCurrentPage().PageNo,
                    PubNumber = TMItem.PubNumber,
                    ApplicantDate = TMItem.ApplicantDate,
                    NameCN = TMItem.NameCN,
                    NameEN = TMItem.NameEN,
                    TMImage = TMItem.TMImage,
                    NameType = (byte?)TMItem.NameType,
                    ApplicantCN = TMItem.ApplicantCN,
                    ApplicantEN = TMItem.ApplicantEN,
                    AddressCN = TMItem.AddressCN,
                    AddressEN = TMItem.AddressEN,
                    AgentCN = TMItem.AgentCN,
                    AgentEN = TMItem.AgentEN,
                    Status = (byte?)sresult,
                    UserId = (status.HasValue && status.Value == TMStatus.EidtSubmit) ? LoginService.GetAccount().Id : TMItem.UserId,
                    RejectReason = !string.IsNullOrEmpty(rejectReason) ? rejectReason : TMItem.RejectReason,
                    Remark = TMItem.Remark,


                    Categories = TMItem.Categories.Select(x =>
                       {
                           return new Category
                           {
                               Id = x.Id,
                               TMElement_Id = TMItem.Id,
                               CategoryNumber = x.Category,
                               Description = x.Description
                           };
                       }).ToList()
                };
                TMItem.Copy(this.TrademarkFeedService.Save(tm));
                if (status.HasValue)
                    this.EventAggregator.GetEvent<TrademarkSaveEvent>().Publish(status);
            }
        }

        private TMStatus? GetStatus(TMStatus? org, TMStatus? targ)
        {
            if (targ == null)
                return org;
            if (targ == TMStatus.QAReturn || targ == TMStatus.QAPass || targ == TMStatus.EidtSubmit)
            {
                if ((org.Value | TMStatus.Load) == org.Value)
                    return TMStatus.Load | targ;
                return TMStatus.Add | targ;
            }

            throw new NotImplementedException();
        }

        private void Crop(object obj)
        {
            Caputre.StartCaputre(30);
        }

        private void Caputre_ScreenCaputred(object sender, ScreenCaputredEventArgs e)
        {
            TMItem.TMImage = e.Bmp.ImageToBytes();
        }

        private void FindTrademark(string pubNumber)
        {
            if (!string.IsNullOrEmpty(pubNumber))
            {
                var tm = TrademarkFeedService.Find(pubNumber);

                if (null != tm && tm.Status != 1)
                {
                    this.ConfirmationRequest.Raise(
                    new Confirmation
                    {
                        Content = string.Format("This record have been {0}, if countinue, will only copy the cotent to new add record!", tm.Status == 0 ? "Deleted" : string.Format("Added to page {0} ", tm.PageNo)),
                        Title = "Confirmation"
                    },
                    c =>
                    {
                        if (c.Confirmed)
                        {
                            TMItem.CopyWithoutId(tm);
                        }
                    });
                }
                else if (null != tm)
                {
                    TMItem.Copy(tm);
                }
            }
        }

        public void TMSelectChanged(TMItem tmItem)
        {
            this.TMItem = tmItem;
        }

        public TMItem TMItem
        {
            get
            {
                return tmItem;
            }

            set
            {
                SetProperty(ref this.tmItem, value);
                (this.SaveCommand as DelegateCommand<TMStatus?>).RaiseCanExecuteChanged();
            }
        }

        public Suggestion NameSelectedItem
        {
            get { return nameSelectedItem; }
            set
            {
                if (value != null && value != nameSelectedItem)
                {
                    SetProperty(ref this.nameSelectedItem, value);
                    TMItem.NameEN = value.EN;
                }
            }
        }

        public Suggestion ApplicantSelectedItem
        {
            get { return applicantSelectedItem; }
            set
            {
                if (value != null && value != applicantSelectedItem)
                {
                    SetProperty(ref this.applicantSelectedItem, value);
                    TMItem.ApplicantEN = value.EN;
                }
            }
        }

        public Suggestion AgentSelectItem
        {
            get { return agentSelectItem; }
            set
            {
                if (value != null && value != agentSelectItem)
                {
                    SetProperty(ref this.agentSelectItem, value);
                    TMItem.AgentEN = value.EN;
                }
            }
        }

        public Suggestion AddressSelectItem
        {
            get { return addressSelectItem; }
            set
            {
                if (value != null && value != addressSelectItem)
                {
                    SetProperty(ref this.addressSelectItem, value);
                    TMItem.AgentEN = value.EN;
                }
            }
        }

        public ICommand FindTrademarkCommand { get { return this.findTrademarkCommand; } }
        public ICommand CropCommand { get { return this.cropCommand; } }
        public ICommand CleanCropCommand { get { return this.cleanCropCommand; } }
        public ICommand SaveCommand { get { return this.saveCommand; } }

        public ISuggestionProvider NameProvider
        {
            get
            {
                if (null != nameProvider)
                    return nameProvider;
                else
                {
                    nameProvider = new SuggestionProvider((string arg) =>
                    {
                        return ConfigurationService.Get(ConfigureType.Name, arg);
                    });

                    return nameProvider;
                }
            }
        }

        public ISuggestionProvider ApplicantProvider
        {
            get
            {
                if (null != applicantProvider)
                    return applicantProvider;
                else
                {
                    applicantProvider = new SuggestionProvider((string arg) =>
                    {
                        return ConfigurationService.Get(ConfigureType.Applicant, arg);
                    });

                    return applicantProvider;
                }
            }
        }

        public ISuggestionProvider AgentProvider
        {
            get
            {
                if (null != agentProvider)
                    return agentProvider;
                else
                {
                    agentProvider = new SuggestionProvider((string arg) =>
                    {
                        return ConfigurationService.Get(ConfigureType.Agent, arg);
                    });

                    return agentProvider;
                }
            }
        }

        public ISuggestionProvider AddressProvider
        {
            get
            {
                if (null != addressProvider)
                    return addressProvider;
                else
                {
                    addressProvider = new SuggestionProvider((string arg) =>
                    {
                        return ConfigurationService.Get(ConfigureType.Address, arg);
                    });

                    return addressProvider;
                }
            }
        }

        private Suggestion nameSelectedItem;
        private Suggestion applicantSelectedItem;
        private Suggestion agentSelectItem;
        private Suggestion addressSelectItem;

        private ISuggestionProvider nameProvider;
        private ISuggestionProvider applicantProvider;
        private ISuggestionProvider agentProvider;
        private ISuggestionProvider addressProvider;
        private readonly IConfigurationService ConfigurationService;
    }
}
