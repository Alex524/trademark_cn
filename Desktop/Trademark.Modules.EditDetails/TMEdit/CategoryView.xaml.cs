﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using WpfControls.Editors;

namespace Trademark.Modules.EditDetails.TMEdit
{
    /// <summary>
    /// Interaction logic for CategoryView.xaml
    /// </summary>
    //[ViewExport(RegionName = RegionNames.CategoryRegion)]
    //[PartCreationPolicy(CreationPolicy.Shared)]
    public partial class CategoryView : UserControl
    {
        //[Import]
        //private IConfigurationService ConfigurationService;

        public CategoryView()
        {
            InitializeComponent();
            //this.CategoryTextBox.Provider = new SuggestionProvider(CategorySeach);
        }

        //private IEnumerable CategorySeach(string filter)
        //{
        //   return ((CategoryViewModel)this.DataContext).ConfigurationService.Get(ConfigureType.Category, filter);

        //    //return ConfigurationService.Get(ConfigureType.Category, filter);
        //}

        //[Import]
        //CategoryViewModel ViewModel
        //{
        //    set { this.DataContext = value; }
        //}


        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.CategoriesList.SelectedItem != null)
            {
                var storyboard = (Storyboard)this.Resources["Details"];
                storyboard.Begin();
            }
            else
            {
                var storyboard = (Storyboard)this.Resources["List"];
                storyboard.Begin();
            }
        }
    }
}
