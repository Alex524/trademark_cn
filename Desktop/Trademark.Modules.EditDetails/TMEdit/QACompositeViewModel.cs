﻿using Prism.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;

namespace Trademark.Modules.EditDetails.TMEdit
{
    [Export(typeof(QACompositeViewModel))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class QACompositeViewModel
    {
        private readonly EditTrademarkViewModel editTrademarkViewModel;
        private readonly CategoryViewModel categoryViewModel;
        private readonly IEventAggregator EventAggregator;

        [ImportingConstructor]
        public QACompositeViewModel(EditTrademarkViewModel editTrademarkModel, CategoryViewModel categoryViewModel, IEventAggregator eventAggregator )
        {
            this.editTrademarkViewModel = editTrademarkModel;
            this.categoryViewModel = categoryViewModel;
            this.EventAggregator = eventAggregator;
            this.EventAggregator.GetEvent<TMQASelectedEvent>().Subscribe(editTrademarkModel.TMSelectChanged);
            this.categoryViewModel.SetTMModel(editTrademarkViewModel);
            this.editTrademarkViewModel.PropertyChanged += EditTrademarkViewModel_PropertyChanged;

        }

        private void EditTrademarkViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TMItem" && null != this.TrademarkModel.TMItem)
                this.CategoryModel.Categories = this.TrademarkModel.TMItem.Categories;
        }

        public EditTrademarkViewModel TrademarkModel { get { return this.editTrademarkViewModel; } }
        public CategoryViewModel CategoryModel { get { return this.categoryViewModel; } }
    }
}
