﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;
using Trademark.Service.Implement.Repositories;

namespace Trademark.Service.Implement
{
    public class UserService : IUserService
    {
        UserRepository userRepository = new UserRepository();

        public IList<User> GetAllUsers()
        {
            return userRepository.GetAllUsers();
        }

        public User Login(string name, string password)
        {
            return userRepository.Login(name, password);
        }

        public void Save(User user)
        {
            userRepository.Save(user);
        }
    }
}
