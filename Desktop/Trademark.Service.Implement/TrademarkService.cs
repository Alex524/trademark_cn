﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Trademark.Service.Contract;
using Trademark.Service.Implement.Repositories;

namespace Trademark.Service.Implement
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class TrademarkService : ITrademarkService
    {
        private static TrademarkRepository repository;

        static TrademarkService()
        {
            repository = new TrademarkRepository();
        }

        public TMElement Find(string pubNumber, int? ggqh)
        {
            return repository.Find(pubNumber, ggqh);
        }

        public TMElement GetById(int id)
        {
            return repository.GetById(id);
        }

        public TMElement GetLastProcessing(int userId)
        {
            return repository.GetLastProcessing(userId);
        }

        public void Remove(int? id)
        {
            repository.Remove(id);
        }

        public TMElement Save(TMElement tm)
        {
            return repository.Save(tm);
        }

        public IList<TMElement> Search(SearchModel model)
        {
            return repository.Search(model);
        }
    }
}
