﻿using LH.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Service.Implement.Repositories
{
    public enum NameProcessType : byte
    {
        TR = 1, QA = 2
    }
    public class NameRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="processType"></param>
        /// <param name="pubno">GGQH</param>
        /// <param name="type">1-OWNN, 2-OWNA, 3-CORN, 4-DESC</param>
        /// <param name="userid"></param>
        /// <param name="category">Only for type 4-DESC</param>
        /// <returns></returns>
        public TMName GetName(NameProcessType processType, int pubno, int type, int? userid, int? category)
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_GetName");
            db.AddInParameter(cmd, "ProcessType", System.Data.DbType.Byte, processType);
            db.AddInParameter(cmd, "PubNo", System.Data.DbType.Int32, pubno);
            db.AddInParameter(cmd, "Type", System.Data.DbType.Int32, type);
            db.AddInParameter(cmd, "UserId", System.Data.DbType.Int32, userid);
            db.AddInParameter(cmd, "Category", System.Data.DbType.Int32, category);
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(delegate (DataRow dr)
            {
                return new TMName
                {
                    ID = dr.Get<int>("ID"),
                    TMQH = dr.Get<int?>("TMQH"),
                    Type = dr.Get<int?>("Type"),
                    CN = dr.Get<string>("CN"),
                    EN = dr.Get<string>("EN"),
                    TMID = dr.Get<int?>("TMID"),
                    Category = dr.Get<int?>("Category"),
                    Status = dr.Get<int?>("Status"),
                    TRUserID = dr.Get<int?>("TRUserID"),
                    QAUserID = dr.Get<int?>("QAUserID"),
                    TRDate = dr.Get<DateTime?>("TRDate"),
                    QADate = dr.Get<DateTime?>("QADate"),
                };
            }).FirstOrDefault();
        }

        internal void Update(TMName name)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_UpdateName");
            db.AddInParameter(cmd, "ID", System.Data.DbType.Int32, name.ID);
            db.AddInParameter(cmd, "EN", System.Data.DbType.String, name.EN);
            db.AddInParameter(cmd, "Status", System.Data.DbType.Int32, name.Status);
            db.ExecuteNonQuery(cmd);
        }

        internal void Reject(TMName name)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_RejectName");
            db.AddInParameter(cmd, "ID", System.Data.DbType.Int32, name.ID);
            db.AddInParameter(cmd, "Reason", System.Data.DbType.String, name.Reason);
            db.ExecuteNonQuery(cmd);
        }
    }
}
