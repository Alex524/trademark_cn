﻿using LH.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Service.Implement.Repositories
{
    public class UserRepository
    {
        public User Login(string name, string Password)
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_Login");
            db.AddInParameter(cmd, "Name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "Password", System.Data.DbType.String, Password);
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(Convert2Model).FirstOrDefault();

        }

        internal IList<User> GetAllUsers()
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_Users");
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(Convert2Model).ToList();
        }

        private User Convert2Model(DataRow dr)
        {
            return new User
            {
                Id = dr.Get<int>("Id"),
                Name = dr.Get<string>("Name"),
                DisplayName = dr.Get<string>("DisplayName"),
                Password = dr.Get<string>("Password"),
                Role = dr.Get<int?>("Role"),
                IsActive = dr.Get<bool?>("IsActive"),
            };
        }

        public void Save(User user)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_UserSave");
            db.AddInParameter(cmd, "Id", System.Data.DbType.Int32, user.Id);
            db.AddInParameter(cmd, "Name", System.Data.DbType.String, user.Name);
            db.AddInParameter(cmd, "DisplayName", System.Data.DbType.String, user.DisplayName);
            db.AddInParameter(cmd, "Password", System.Data.DbType.String, user.Password);
            db.AddInParameter(cmd, "IsActive", System.Data.DbType.Boolean, user.IsActive);
            db.AddInParameter(cmd, "Role", System.Data.DbType.Int32, user.Role);
            db.ExecuteNonQuery(cmd);
        }
    }
}
