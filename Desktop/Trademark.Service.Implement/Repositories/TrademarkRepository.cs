﻿using LH.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Service.Implement.Repositories
{
    public class TrademarkRepository
    {
        public TMElement GetLastProcessing(int userId)
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_LastProcessing");
            db.AddInParameter(cmd, "UserId", System.Data.DbType.Int32, userId);
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(Convert2Model).FirstOrDefault();

        }

        internal void Remove(int? id)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_Remove");
            db.AddInParameter(cmd, "Id", System.Data.DbType.Int32, id);
            db.ExecuteNonQuery(cmd);
        }

        internal TMElement GetById(int id)
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_GetTMById");
            db.AddInParameter(cmd, "ID", System.Data.DbType.Int32, id);
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(Convert2Model).FirstOrDefault();
        }

        internal TMElement Find(string pubNumber, int? ggqh)
        {
            DataSet ds = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_Find");
            db.AddInParameter(cmd, "PubNumber", System.Data.DbType.String, pubNumber);
            db.AddInParameter(cmd, "GGQH", System.Data.DbType.Int32, ggqh);
            ds = db.ExecuteDataSet(cmd);

            var element = ds.Tables[0].AsEnumerable().Select(Convert2Model).FirstOrDefault();

            if (ds.Tables[1].Rows.Count > 0)
                element.Categories = ds.Tables[1].Select(string.Format("TMElement_Id = {0}", element.Id)).AsEnumerable().Select(Convert2CategoryModel).ToList();

            return element;
        }

        public void SaveTMTrans(IList<TMElement> tms)
        {
            var dtTMTrans = new DataTable
            {
                Columns =
                {
                    { "TMQH", typeof(int) }, { "Type", typeof(int) }, {"CN", typeof(string) }, {"EN", typeof(string) }, { "TMID" ,typeof(int) } , {"Category", typeof(int) }
                }
            };

            DataRow dr = null;

            foreach (var tm in tms)
            {
                foreach (var item in Split(tm.ApplicantCN, tm.ApplicantEN))
                {
                    dr = dtTMTrans.NewRow();
                    dr.SetField<int?>("TMQH", tm.GGQH);
                    dr.SetField<int?>("Type", 1);
                    dr.SetField<string>("CN", item.CN);
                    dr.SetField<string>("EN", item.EN);
                    dr.SetField<int?>("TMID", tm.Id);
                    dtTMTrans.Rows.Add(dr);
                }

                foreach (var item in Split(tm.AddressCN, tm.AddressEN))
                {
                    dr = dtTMTrans.NewRow();
                    dr.SetField<int?>("TMQH", tm.GGQH);
                    dr.SetField<int?>("Type", 2);
                    dr.SetField<string>("CN", item.CN);
                    dr.SetField<string>("EN", item.EN);
                    dr.SetField<int?>("TMID", tm.Id);
                    dtTMTrans.Rows.Add(dr);
                }

                foreach (var item in Split(tm.AgentCN, tm.AgentEN))
                {
                    dr = dtTMTrans.NewRow();
                    dr.SetField<int?>("TMQH", tm.GGQH);
                    dr.SetField<int?>("Type", 3);
                    dr.SetField<string>("CN", item.CN);
                    dr.SetField<string>("EN", item.EN);
                    dr.SetField<int?>("TMID", tm.Id);
                    dtTMTrans.Rows.Add(dr);
                }

                foreach (var category in tm.Categories)
                {
                    foreach (var item in Split(category.Description, null))
                    {
                        dr = dtTMTrans.NewRow();
                        dr.SetField<int?>("TMQH", tm.GGQH);
                        dr.SetField<int?>("Type", 4);
                        dr.SetField<string>("CN", item.CN);
                        dr.SetField<string>("EN", item.EN);
                        dr.SetField<int?>("TMID", tm.Id);
                        dr.SetField<int?>("Category", category.CategoryNumber);
                        dtTMTrans.Rows.Add(dr);
                    }
                }
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["DbHelperConnectionString"]))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("P_SaveTMTrans", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 5999;
                    cmd.Parameters.AddWithValue("@TMTrans", dtTMTrans);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        internal class TranType
        {
            public int? Type { get; set; }
            public int? Category { get; set; }
            public string CN { get; set; }
            public string EN { get; set; }
            public string Corrected_CN { get; set; }
        }

        private IList<TranType> Split(string CN, string EN)
        {

            TranType[] result = null;

            if (!string.IsNullOrEmpty(CN))
            {
                var str = Regex.Replace(CN, @"[\s]+", " ", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var items = Regex.Split(str, "[；；;;]", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                result = items.Where(x => !string.IsNullOrEmpty(x.Trim())).Select(delegate (string x)
                {
                    return new TranType { CN = x.Trim() };
                }).ToArray();
            }

            if (!string.IsNullOrEmpty(EN))
            {
                var str = Regex.Replace(EN, @"[\s]+", " ", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var items = Regex.Split(str, "[；；;;]", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var ens = items.Where(x => !string.IsNullOrEmpty(x.Trim())).Select(delegate (string x)
                {
                    return new TranType { EN = x.Trim() };
                }).ToArray();

                if (ens != null)
                {
                    for (int i = 0; i < ens.Length; i++)
                    {
                        result[i].EN = ens[i].EN;
                    }
                }
            }

            if (result != null)
                return result;
            else return new List<TranType>();
        }

        private string CNFix(string cn, IEnumerable<TranType> trans)
        {
            string result = null;
            if (!string.IsNullOrEmpty(cn))
            {
                var str = Regex.Replace(cn, @"[\s]+", " ", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var items = str.Split(new char[] { '；', '；', ';', ';' }, StringSplitOptions.RemoveEmptyEntries);


                foreach (var item in items)
                {

                    var transtype = trans.FirstOrDefault(x => CultureInfo.InvariantCulture.CompareInfo.Compare(x.CN, item.Trim(), CompareOptions.IgnoreSymbols | CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) == 0
                   || (!string.IsNullOrEmpty(x.Corrected_CN) && CultureInfo.InvariantCulture.CompareInfo.Compare(x.Corrected_CN, item.Trim(), CompareOptions.IgnoreSymbols | CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) == 0));

                    if (transtype != null)
                    {
                        result += !string.IsNullOrEmpty(transtype.Corrected_CN) ? transtype.Corrected_CN.Trim() : transtype.CN.Trim() + "；";
                    }
                    else
                        result += item;
                }
            }
            result = Regex.Replace(!String.IsNullOrEmpty(result) ? result : "", @"[\s]+", " ");

            return !string.IsNullOrEmpty(result) ? result.Trim().TrimEnd('；') : null;
        }

        private string MergeEN(int type, string cn, IEnumerable<TranType> trans)
        {
            string result = null;
            bool isfirst = true;
            if (!string.IsNullOrEmpty(cn))
            {
                var str = Regex.Replace(cn, @"[\s]+", " ", RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                var items = str.Split(new char[] { '；', '；', ';', ';' }, StringSplitOptions.RemoveEmptyEntries);


                foreach (var item in items)
                {

                    var transtype = trans.FirstOrDefault(x => CultureInfo.InvariantCulture.CompareInfo.Compare(x.CN, item.Trim(), CompareOptions.IgnoreSymbols | CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) == 0
                   || (!string.IsNullOrEmpty(x.Corrected_CN) && CultureInfo.InvariantCulture.CompareInfo.Compare(x.Corrected_CN, item.Trim(), CompareOptions.IgnoreSymbols | CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace) == 0));
                    if (transtype != null)
                    {
                        if (isfirst && type == 4)
                        {
                            var fist = transtype.EN.Trim().First();
                            var left = transtype.EN.Trim().TrimStart(fist);

                            result += fist.ToString().ToUpper() + left.ToLower() + "; ";
                            isfirst = false;
                        }
                        else
                            result += transtype.EN.Trim() + "; ";

                    }
                    else
                        result += "";
                    //throw new IndexOutOfRangeException();
                }
            }

            result = Regex.Replace(!String.IsNullOrEmpty(result) ? result : "", @"[\s]+", " ");
            return !string.IsNullOrEmpty(result) ? result.Trim().TrimEnd(';') : null;
        }

        internal TMElement Save(TMElement tm)
        {
            #region Table Definiation
            var dtTM = new DataTable
            {
                Columns = {
                    { "Id", typeof(int) }, { "GGRQ", typeof(DateTime) }, { "GGQH", typeof(int) }, { "PageNo", typeof(int) }, { "PubNumber", typeof(string) }, { "ApplicantDate", typeof(DateTime) },
                    { "NameCN", typeof(string) }, { "NameEN", typeof(string) },{ "TMImage", typeof(byte[]) }, { "NameType", typeof(byte) },{ "ApplicantCN", typeof(string) },{ "ApplicantEN", typeof(string) },
                    { "AddressCN", typeof(string) },{ "AddressEN", typeof(string) }, { "AgentCN", typeof(string) },{ "AgentEN", typeof(string) },
                    { "Status", typeof(int) }, { "UserId", typeof(int) }, { "LastUpdate", typeof(DateTime) }, { "RejectReason", typeof(string) }, { "Remark", typeof(string) }
                }
            };

            var dtCategory = new DataTable
            {
                Columns = {
                    { "Id", typeof(int) },
                    { "TMElement_Id", typeof(int) },
                    { "CategoryNumber", typeof(int) },
                    { "Description", typeof(string) }
                }
            };
            #endregion


            var dr = dtTM.NewRow();
            dr.SetField<int?>("Id", tm.Id);
            dr.SetField<int?>("GGQH", tm.GGQH);
            dr.SetField<DateTime?>("GGRQ", tm.GGRQ);
            dr.SetField<int?>("PageNo", tm.PageNo);
            dr.SetField<string>("PubNumber", tm.PubNumber);
            dr.SetField<DateTime?>("ApplicantDate", tm.ApplicantDate);
            dr.SetField<string>("NameCN", tm.NameCN);
            dr.SetField<string>("NameEN", tm.NameEN);
            dr.SetField<byte[]>("TMImage", tm.TMImage);
            dr.SetField<byte?>("NameType", tm.NameType);
            dr.SetField<string>("ApplicantCN", tm.ApplicantCN);
            dr.SetField<string>("ApplicantEN", tm.ApplicantEN);
            dr.SetField<string>("AddressCN", tm.AddressCN);
            dr.SetField<string>("AddressEN", tm.AddressEN);
            dr.SetField<string>("AgentCN", tm.AgentCN);
            dr.SetField<string>("AgentEN", tm.AgentEN);
            dr.SetField<int?>("Status", tm.Status);
            dr.SetField<int?>("UserId", tm.UserId);
            dr.SetField<string>("RejectReason", tm.RejectReason);
            dr.SetField<string>("Remark", tm.Remark);
            dr.SetField<DateTime?>("LastUpdate", tm.LastUpdate);

            dtTM.Rows.Add(dr);
            if (tm.Categories != null)
            {
                DataRow drCat = null;
                foreach (var cat in tm.Categories)
                {
                    drCat = dtCategory.NewRow();
                    drCat.SetField<int?>("Id", cat.Id);
                    drCat.SetField<int?>("TMElement_Id", cat.TMElement_Id);
                    drCat.SetField<int?>("CategoryNumber", cat.CategoryNumber);
                    drCat.SetField<string>("Description", cat.Description);
                    dtCategory.Rows.Add(drCat);
                }
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["DbHelperConnectionString"]))
            {
                conn.Open();


                using (SqlCommand cmd = new SqlCommand("P_SaveTM", conn))
                {
                    DataSet ds = new DataSet();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TM", dtTM);
                    cmd.Parameters.AddWithValue("@Category", dtCategory);
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(ds);

                        var element = ds.Tables[0].AsEnumerable().Select(Convert2Model).FirstOrDefault();

                        if (ds.Tables[1].Rows.Count > 0)
                            element.Categories = ds.Tables[1].Select(string.Format("TMElement_Id = {0}", element.Id)).AsEnumerable().Select(Convert2CategoryModel).ToList();

                        return element;
                    }
                }


            }


        }

        public IList<TMElement> SampleGenerate()
        {
            DataSet ds = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("SampleFileGenerate");
            ds = db.ExecuteDataSet(cmd);

            return ds.Tables[0].AsEnumerable().Select(delegate (DataRow s)
            {
                var trans = ds.Tables[2].Select(string.Format("TMID = {0}", s.Get<int>("Id"))).Select(delegate (DataRow r)
                {
                    return new TranType
                    {
                        CN = r.Get<string>("CN"),
                        EN = r.Get<string>("EN"),
                        Corrected_CN = r.Get<string>("Corrected_CN"),

                    };
                });

                return new TMElement
                {
                    Id = s.Get<int>("Id"),
                    GGRQ = s.Get<DateTime?>("GGRQ"),
                    PubNumber = s.Get<string>("PubNumber"),
                    PageNo = s.Get<int?>("PageNo"),
                    ApplicantDate = s.Get<DateTime?>("ApplicantDate"),
                    ApplicantCN = CNFix(s.Get<string>("ApplicantCN"), trans),
                    ApplicantEN = MergeEN(1, s.Get<string>("ApplicantCN"), trans),
                    AddressCN = s.Get<string>("AddressCN"),
                    AddressEN = MergeEN(2, s.Get<string>("AddressCN"), trans),
                    AgentCN = CNFix(s.Get<string>("AgentCN"), trans),
                    AgentEN = MergeEN(3, s.Get<string>("AgentCN"), trans),
                    NameCN = s.Get<string>("NameCN"),
                    NameEN = s.Get<string>("NameEN"),
                    NameType = s.Get<byte?>("NameType"),
                    TMImage = s.Get<byte[]>("TMImage"),
                    OWNC = s.Get<string>("OWNC"),

                    Categories = ds.Tables[1].Select(string.Format("TMElement_Id = {0}", s.Get<int>("Id"))).AsEnumerable().Select(delegate (DataRow c)
                    {
                        return new Category
                        {
                            TMElement_Id = c.Get<int?>("TMElement_Id"),
                            CategoryNumber = c.Get<int?>("Category"),
                            Description = CNFix(c.Get<string>("Description"), trans),
                            DescriptionEN = MergeEN(4, c.Get<string>("Description"), trans)
                        };
                    }).ToList()

                };
            }).ToList();
            //return ds.Tables[0].AsEnumerable().Select(Convert2Model).FirstOrDefault();
        }

       

        public IList<TMElement> GetTMTrans()
        {
            DataSet ds = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_GetTMTrans");
            ds = db.ExecuteDataSet(cmd);

            return ds.Tables[0].AsEnumerable().Select(delegate (DataRow s)
            {
                return new TMElement
                {
                    Id = s.Get<int>("Id"),
                    GGQH = s.Get<int>("GGQH"),
                    ApplicantCN = s.Get<string>("ApplicantCN"),
                    ApplicantEN = s.Get<string>("ApplicantEN"),
                    AddressCN = s.Get<string>("AddressCN"),
                    AddressEN = s.Get<string>("AddressEN"),
                    AgentCN = s.Get<string>("AgentCN"),
                    AgentEN = s.Get<string>("AgentEN"),

                    Categories = ds.Tables[1].Select(string.Format("TMElement_Id = {0}", s.Get<int>("Id"))).AsEnumerable().Select(delegate (DataRow c)
                    {
                        return new Category
                        {
                            TMElement_Id = c.Get<int?>("TMElement_Id"),
                            CategoryNumber = c.Get<int?>("Category"),
                            Description = c.Get<string>("Description"),
                        };
                    }).ToList()

                };
            }).ToList();
        }

        private void SaveCategory(Category category, Trans t, int tm_id)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_SaveCategoriy");
            db.AddInParameter(cmd, "Id", System.Data.DbType.Int32, category.Id);
            db.AddInParameter(cmd, "TM_Id", System.Data.DbType.Int32, tm_id);
            db.AddInParameter(cmd, "Category", System.Data.DbType.Int32, category.CategoryNumber);
            db.AddInParameter(cmd, "Description", System.Data.DbType.String, category.Description);
            db.ExecuteScalar(cmd, t);
        }

        //private int SaveTM(TMElement tm, Trans t)
        //{
        //    SqlUtility db = new SqlUtility();
        //    DbCommand cmd = db.GetStoredProcCommond("P_SaveTM");
        //    db.AddInParameter(cmd, "Id", System.Data.DbType.Int32, tm.Id);
        //    db.AddInParameter(cmd, "GGRQ", System.Data.DbType.DateTime, tm.GGRQ);
        //    db.AddInParameter(cmd, "GGQH", System.Data.DbType.Int32, tm.GGQH);
        //    db.AddInParameter(cmd, "PageNo", System.Data.DbType.Int32, tm.PageNo);
        //    db.AddInParameter(cmd, "PubNumber", System.Data.DbType.String, tm.PubNumber);
        //    db.AddInParameter(cmd, "ApplicantDate", System.Data.DbType.DateTime, tm.ApplicantDate);
        //    db.AddInParameter(cmd, "NameCN", System.Data.DbType.String, tm.NameCN);
        //    db.AddInParameter(cmd, "NameEN", System.Data.DbType.String, tm.NameEN);
        //    db.AddInParameter(cmd, "TMImage", DbType.Binary, tm.TMImage);
        //    db.AddInParameter(cmd, "ApplicantCN", DbType.String, tm.ApplicantCN);
        //    db.AddInParameter(cmd, "ApplicantEN", DbType.String, tm.ApplicantEN);
        //    db.AddInParameter(cmd, "AddressCN", DbType.String, tm.AddressCN);
        //    db.AddInParameter(cmd, "AddressEN", DbType.String, tm.AddressEN);
        //    db.AddInParameter(cmd, "Status", DbType.Int32, tm.Status);
        //    db.AddInParameter(cmd, "UserId", DbType.Int32, tm.UserId);
        //    db.AddInParameter(cmd, "LastUpdate", DbType.DateTime, DateTime.Now);
        //    return (int)db.ExecuteScalar(cmd, t);
        //}

        private Category Convert2CategoryModel(DataRow dr)
        {
            return new Category
            {
                Id = dr.Get<int>("Id"),
                TMElement_Id = dr.Get<int?>("TMElement_Id"),
                CategoryNumber = dr.Get<int?>("Category"),
                Description = dr.Get<string>("Description"),
            };
        }

        private TMElement Convert2Model(DataRow dr)
        {
            return new TMElement
            {
                Id = dr.Get<int>("Id"),
                GGRQ = dr.Get<DateTime?>("GGRQ"),
                GGQH = dr.Get<int?>("GGQH"),
                PageNo = dr.Get<int?>("PageNo"),
                PubNumber = dr.Get<string>("PubNumber"),
                ApplicantDate = dr.Get<DateTime?>("ApplicantDate"),
                NameCN = dr.Get<string>("NameCN"),
                NameEN = dr.Get<string>("NameEN"),
                TMImage = dr.Get<byte[]>("TMImage"),
                NameType = dr.Get<byte?>("NameType"),
                ApplicantCN = dr.Get<string>("ApplicantCN"),
                ApplicantEN = dr.Get<string>("ApplicantEN"),
                AddressCN = dr.Get<string>("AddressCN"),
                AddressEN = dr.Get<string>("AddressEN"),
                AgentCN = dr.Get<string>("AgentCN"),
                AgentEN = dr.Get<string>("AgentEN"),
                Status = dr.Get<int?>("Status"),
                UserId = dr.Get<int?>("UserId"),
                LastUpdate = dr.Get<DateTime?>("LastUpdate"),
                RejectReason = dr.Get<string>("RejectReason"),
                Remark = dr.Get<string>("Remark"),
                DisplayName = dr.Get<string>("DisplayName"),
            };
        }

        internal IList<TMElement> Search(SearchModel model)
        {
            DataSet ds = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_Search");
            db.AddInParameter(cmd, "GGRQ", System.Data.DbType.DateTime, model.GGRQ);
            db.AddInParameter(cmd, "GGQH", System.Data.DbType.Int32, model.GGQH);
            db.AddInParameter(cmd, "PageNo", System.Data.DbType.Int32, model.PageNo);
            db.AddInParameter(cmd, "UserId", System.Data.DbType.Int32, model.UserId);
            ds = db.ExecuteDataSet(cmd);

            var result = ds.Tables[0].AsEnumerable().Select(Convert2Model).ToList();

            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                foreach (var tm in result)
                {
                    tm.Categories = ds.Tables[1].Select(string.Format("TMElement_Id = {0}", tm.Id)).AsEnumerable().Select(Convert2CategoryModel).ToList();
                }

            return result;
        }
    }
}
