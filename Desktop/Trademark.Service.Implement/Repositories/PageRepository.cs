﻿using LH.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;

namespace Trademark.Service.Implement.Repositories
{
    class PageRepository
    {
        internal TMPage GetPage(int? userId, int? pubNo, int? fromNo, int? toNo)
        {
            DataTable dt = null;
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_QAProcessing");
            db.AddInParameter(cmd, "UserId", System.Data.DbType.Int32, userId);
            db.AddInParameter(cmd, "PubNo", System.Data.DbType.Int32, pubNo);
            db.AddInParameter(cmd, "FromNo", System.Data.DbType.Int32, fromNo);
            db.AddInParameter(cmd, "ToNo", System.Data.DbType.Int32, toNo);
            dt = db.ExecuteDataTable(cmd);

            return dt.AsEnumerable().Select(Convert2Model).FirstOrDefault();
        }

        private TMPage Convert2Model(DataRow dr)
        {
            return new TMPage
            {
                Id = dr.Get<int>("Id"),
                PubDate = dr.Get<DateTime?>("PubDate"),
                PubNo = dr.Get<int?>("PubNo"),
                Page = dr.Get<int?>("PageNo"),
                Status = dr.Get<byte?>("Status"),
                UserId = dr.Get<int?>("UserId"),
            };
        }

        internal void Update(int id, byte status)
        {
            SqlUtility db = new SqlUtility();
            DbCommand cmd = db.GetStoredProcCommond("P_PageUpdate");
            db.AddInParameter(cmd, "Id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "Status", System.Data.DbType.Byte, status);
            db.ExecuteNonQuery(cmd);
        }
    }
}
