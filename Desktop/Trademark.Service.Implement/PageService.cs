﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;
using Trademark.Service.Implement.Repositories;

namespace Trademark.Service.Implement
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PageService : IPageService
    {
        private PageRepository repository;
        public PageService()
        {
            repository = new PageRepository();
        }
        public TMPage GetPage(int? userId, int? pubNo, int? fromNo, int? toNo)
        {
            return repository.GetPage(userId, pubNo, fromNo, toNo);
        }

        public void Update(int pageid, byte status)
        {
            repository.Update(pageid, status);
        }
    }
}
