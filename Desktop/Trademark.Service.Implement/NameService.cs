﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;
using Trademark.Service.Implement.Repositories;

namespace Trademark.Service.Implement
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class NameService : INameService
    {
        static NameRepository repository;

        static NameService()
        {
            repository = new NameRepository();
        }
        public TMName GetQAName(int pubno, int type, int? userid, int? category)
        {
            return repository.GetName(NameProcessType.TR, pubno, type, userid, category);
        }

        public TMName GetTRName(int pubno, int type, int? userid, int? category)
        {
            return repository.GetName(NameProcessType.QA, pubno, type, userid, category);
        }

        public void Reject(TMName name)
        {
            repository.Reject(name);
        }

        public void Update(TMName name)
        {
            repository.Update(name);
        }
    }
}
