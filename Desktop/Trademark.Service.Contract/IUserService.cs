﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Service.Contract
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        User Login(string name, string password);

        [OperationContract]
        IList<User> GetAllUsers();

        [OperationContract]
        void Save(User user);
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int? Role { get; set; }
        [DataMember]
        public bool? IsActive { get; set; }
    }
}
