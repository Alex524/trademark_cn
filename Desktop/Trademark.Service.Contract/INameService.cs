﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Service.Contract
{
    [ServiceContract]
    public interface INameService
    {
        [OperationContract]
        TMName GetTRName(int pubno, int type, int? userid, int? category);
        [OperationContract]
        TMName GetQAName(int pubno, int type, int? userid, int? category);

        [OperationContract]
        void Update(TMName name);

        [OperationContract]
        void Reject(TMName name);
    }

    [DataContract]
    public class TMName
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int? TMQH { get; set; }
        [DataMember]
        public int? Type { get; set; }  
        [DataMember]
        public string CN { get; set; }
        [DataMember]
        public string EN { get; set; }
        [DataMember]
        public int? TMID { get; set; }
        [DataMember]
        public int? Category { get; set; }
        [DataMember]
        public int? Status { get; set; }
        [DataMember]
        public int? TRUserID { get; set; }
        [DataMember]
        public int? QAUserID { get; set; }
        [DataMember]
        public DateTime? TRDate { get; set; }
        [DataMember]
        public DateTime? QADate { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
