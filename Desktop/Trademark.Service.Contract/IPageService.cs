﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Service.Contract
{
    [ServiceContract]
    public interface IPageService
    {
        [OperationContract]
        TMPage GetPage(int? userId, int? pubNo, int? pageNo, int? toNo);

        [OperationContract]
        void Update(int pageid, byte status);
    }

    [DataContract]
    public class TMPage
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime? PubDate { get; set; }
        [DataMember]
        public int? PubNo { get; set; }
        [DataMember]
        public int? Page { get; set; }
        [DataMember]
        public byte? Status { get; set; }
        [DataMember]
        public int? UserId { get; set; }

    }
}
