﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Service.Contract
{
    [ServiceContract]
    public interface ITrademarkService
    {
        [OperationContract]
        IList<TMElement> Search(SearchModel model);

        [OperationContract]
        TMElement GetLastProcessing(int userId);

        [OperationContract]
        TMElement Find(string pubNumber, int? ggqh);

        [OperationContract]
        TMElement Save(TMElement tm);

        [OperationContract]
        void Remove(int? id);

        [OperationContract]
        TMElement GetById(int id);
    }

    [DataContract]
    public class TMElement
    {
        public TMElement()
        {
            this.Categories = new List<Category>();
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime? GGRQ { get; set; }
        [DataMember]
        public int? GGQH { get; set; }
        [DataMember]
        public int? PageNo { get; set; }
        [DataMember]
        public string PubNumber { get; set; }
        [DataMember]
        public DateTime? ApplicantDate { get; set; }
        [DataMember]
        public string NameCN { get; set; }
        [DataMember]
        public string NameEN { get; set; }
        [DataMember]
        public byte[] TMImage { get; set; }
        [DataMember]
        public byte? NameType { get; set; }
        [DataMember]
        public string ApplicantCN { get; set; }
        [DataMember]
        public string ApplicantEN { get; set; }
        [DataMember]
        public string AddressCN { get; set; }
        [DataMember]
        public string AddressEN { get; set; }
        [DataMember]
        public string AgentCN { get; set; }
        [DataMember]
        public string AgentEN { get; set; }
        [DataMember]
        public int? Status { get; set; }
        [DataMember]
        public int? UserId { get; set; }
        [DataMember]
        public DateTime? LastUpdate { get; set; }

        [DataMember]
        public IList<Category> Categories { get; set; }

        [DataMember]
        public string RejectReason { get; set; }
        [DataMember]
        public string Remark { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        public string OWNC { get; set; }
    }

    [DataContract]
    public class Category
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int? TMElement_Id { get; set; }
        [DataMember]
        public int? CategoryNumber { get; set; }
        [DataMember]
        public string Description { get; set; }
        public string DescriptionEN { get; set; }
    }

    [DataContract]
    public class SearchModel
    {
        [DataMember]
        public int? UserId { get; set; }
        [DataMember]
        public DateTime? GGRQ { get; set; }
        [DataMember]
        public int? GGQH { get; set; }
        [DataMember]
        public int? PageNo { get; set; }
    }
}
