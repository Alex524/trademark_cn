﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Service.Contract;

namespace Trademark.Modules.Edit.Services
{
    [Export(typeof(ITrademarkFeedService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class TrademarkFeedService : ITrademarkFeedService
    {
        private SearchModel currentPage;
        public SearchModel GetCurrentPage()
        {
            return currentPage;
        }

        public void SetCurrentPage(SearchModel model)
        {
            currentPage = model;
        }
        public TMElement GetProcessingPage(int userId)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<ITrademarkService>();
                var result = service.GetLastProcessing(userId);
                return result;
            }
        }

        public IList<TMElement> Search(SearchModel search)
        {
            using (ESBClient client = new ESBClient())
            {

                var service = client.GetClientProxy<ITrademarkService>();
                var result = service.Search(search ?? currentPage);
                return result;
            }
        }

        public TMElement Find(string pubNumber)
        {
            using (ESBClient client = new ESBClient())
            {

                var service = client.GetClientProxy<ITrademarkService>();
                var result = service.Find(pubNumber, currentPage.GGQH);
                return result;
            }
        }

        public TMElement Save(TMElement tm)
        {
            using (ESBClient client = new ESBClient())
            {

                var service = client.GetClientProxy<ITrademarkService>();
                return service.Save(tm);
            }
        }

        public void Remove(int? id)
        {
            using (ESBClient client = new ESBClient())
            {

                var service = client.GetClientProxy<ITrademarkService>();
                service.Remove(id);
            }
        }
    }
}
