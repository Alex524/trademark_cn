﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Service.Contract;

namespace Trademark.Modules.Edit.Services
{
    [Export(typeof(IPageFeedService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class PageFeedService : IPageFeedService
    {
        private static TMPage currentPage;


        public TMPage GetCurrentPage()
        {
            return currentPage;
        }

        public TMPage GetPage(bool refresh)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IPageService>();
                var result = service.GetPage(currentPage.UserId, currentPage.PubNo, refresh ? currentPage.Page : null, null);
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Status">status' value should be 8 or 16, 8 means parts finished. and 16 means all have been finished. after we will update the status to 4 if new updated on this page</param>
        public void UpdatePage(byte Status)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IPageService>();
                service.Update(currentPage.Id, Status);
            }
        }

        public void SetCurrentPage(TMPage page)
        {
            currentPage = page;
        }

        public TMPage GetSpecialPage(int? fromNo, int? toNo)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IPageService>();
                var result = service.GetPage(currentPage.UserId, currentPage.PubNo, fromNo, toNo);
                return result;
            }
        }
    }
}
