﻿using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.Edit
{
    public interface IEditSummaryViewModel: IHeaderInfoProvider<string>
    {
    }
}