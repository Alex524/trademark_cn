﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.Edit.Summary
{
    [Export(typeof(IEditSummaryViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class EditSummaryViewModel : BindableBase, IEditSummaryViewModel
    {
        public string HeaderInfo
        {
            get
            {
                return "TRADEMARK EDIT";
            }
        }
    }
}
