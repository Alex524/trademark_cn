﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;


namespace Trademark.Modules.Edit.Summary
{
    [Export(typeof(TrademarkSearchViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class TrademarkSearchViewModel : BindableBase
    {
        private readonly ILoginService LoginService;
        private readonly IEventAggregator EventAggregator;
        private ObservableCollection<TMItem> tmItems;
        private TMItem currentTM;
        private readonly ITrademarkFeedService TrademarkFeedService;
        private ICommand searchCommand;
        private ICommand nextCommand;
        private readonly ICommand addCommand;
        [ImportingConstructor]
        public TrademarkSearchViewModel(ILoginService loginService, IEventAggregator eventAggregator, ITrademarkFeedService trademarkFeedService)
        {
            this.LoginService = loginService;
            this.EventAggregator = eventAggregator;
            this.TrademarkFeedService = trademarkFeedService;
            this.LoginService.Updated += GetLastEditPage;
            this.searchCommand = new DelegateCommand<TMStatus?>(Search);
            this.nextCommand = new DelegateCommand<string>(Next);
            this.addCommand = new DelegateCommand<string>(Add);
            this.removeCommand = new DelegateCommand<int?>(Remove);
            this.TMItems = new ObservableCollection<TMItem>();
            this.EventAggregator.GetEvent<TrademarkSaveEvent>().Subscribe((item) => { Search(item); });
        }

        private void Remove(int? id)
        {
            this.TrademarkFeedService.Remove(id);
            this.Search(TMStatus.EidtSubmit);

        }

        private void Add(string obj)
        {
            this.CurrentTM = new TMItem { UserId = LoginService.GetAccount().Id, GGQH = this.GGQH, GGRQ = this.GGRQ, PageNo = this.PageNo, Status = TMStatus.Add };
        }

        private void Next(string obj)
        {
            this.PageNo++;
            this.Search(TMStatus.EidtSubmit);
        }

        private void Search(TMStatus? obj)
        {
            if (obj != TMStatus.EidtSubmit)
                return;

            var result = this.TrademarkFeedService.Search(null);

            TMItems.Clear();
            CurrentTM = null;

            if (result != null)
            {
                foreach (var item in result)
                    this.TMItems.Add(new TMItem(item));
            }
        }

        private void GetLastEditPage(object sender, Infrastructure.Models.AccountEventArgs e)
        {
            if (e.Account != null)
            {
                var result = this.TrademarkFeedService.GetProcessingPage(e.Account.Id);
                if (null != result)
                {
                    this.GGQH = result.GGQH;
                    this.GGRQ = result.GGRQ;
                    this.PageNo = result.PageNo;
                }
            }
        }

        private void UpdateCurrentPage()
        {
            TrademarkFeedService.SetCurrentPage(new Service.Contract.SearchModel { PageNo = this.PageNo, GGQH = this.GGQH, GGRQ = this.GGRQ, UserId = LoginService.GetAccount().Id });
        }

        public ObservableCollection<TMItem> TMItems
        {
            get { return this.tmItems; }
            set
            {
                SetProperty(ref this.tmItems, value);
            }
        }

        private int? ggqh;
        public int? GGQH
        {
            get
            {
                return this.ggqh;
            }
            set
            {
                if (value != this.ggqh)
                {
                    SetProperty(ref this.ggqh, value);
                    UpdateCurrentPage();
                    //TrademarkFeedService.GetCurrentPage().GGQH = value;
                }
            }
        }

        public int? PageNo
        {
            get
            {
                return pageNo;
            }

            set
            {
                if (value != this.pageNo)
                {
                    SetProperty(ref this.pageNo, value);
                    UpdateCurrentPage();
                }
            }
        }

        public DateTime? GGRQ
        {
            get
            {
                return _GGRQ;
            }

            set
            {
                if (value != _GGRQ)
                {
                    SetProperty(ref this._GGRQ, value);
                    UpdateCurrentPage();
                }
            }
        }

        private int? pageNo;

        private DateTime? _GGRQ;
        private readonly ICommand removeCommand;

        public ICommand SearchCommand { get { return this.searchCommand; } }
        public ICommand NextCommand { get { return this.nextCommand; } }

        public ICommand AddCommand { get { return this.addCommand; } }

        public ICommand RemoveCommand { get { return this.removeCommand; } }


        public TMItem CurrentTM
        {
            get
            {
                return currentTM;
            }

            set
            {
                if (SetProperty(ref currentTM, value))
                {
                    EventAggregator.GetEvent<TMSelectedEvent>().Publish(
                        currentTM);
                }
            }
        }
    }
}
