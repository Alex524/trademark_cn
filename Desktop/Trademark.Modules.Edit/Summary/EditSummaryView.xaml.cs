﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;

namespace Trademark.Modules.Edit.Summary
{
    /// <summary>
    /// Interaction logic for EditSummaryView.xaml
    /// </summary>
    [ViewExport(RegionName = RegionNames.MainRegion)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class EditSummaryView : UserControl
    {
        public EditSummaryView()
        {
            InitializeComponent();
        }

        [Import]
        public IEditSummaryViewModel Model
        {
            get { return DataContext as IEditSummaryViewModel; }
            set { DataContext = value; }
        }
    }
}
