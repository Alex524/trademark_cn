﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;
using Xceed.Wpf.Toolkit;

namespace Trademark.Modules.Edit.Summary
{
    /// <summary>
    /// Interaction logic for TrademarkSearchView.xaml
    /// </summary>
    [ViewExport(RegionName = RegionNames.QARegion)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class QASearchView : UserControl
    {
        public QASearchView()
        {
            InitializeComponent();
        }

        [Import]
        QASearchViewModel ViewModel
        {
            set { this.DataContext = value; }
        }
    }
}
