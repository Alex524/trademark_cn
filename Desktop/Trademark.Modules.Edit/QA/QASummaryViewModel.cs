﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Modules.Edit.QA
{
    [Export(typeof(IQASummaryViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class QASummaryViewModel : IQASummaryViewModel
    {
        public string HeaderInfo
        {
            get
            {
                return "QA";
            }
        }
    }
}
