﻿using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Modules.Edit.QA
{
    public class SpecialPageNotification : Confirmation
    {
        public SpecialPageNotification()
        {
            this.FromNo = null;
            this.ToNo = null;
        }
        public int? FromNo { get; set; }
        public int? ToNo { get; set; }
    }
}
