﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Trademark.Modules.Edit.QA
{
    public class GetSpecialPageViewModel : BindableBase, IInteractionRequestAware
    {
        private SpecialPageNotification notification;
        private ICommand okCommand;
        private ICommand cancelCommand;
        public Action FinishInteraction { get; set; }

        public INotification Notification
        {
            get
            {
                return this.notification;
            }

            set
            {
                SetProperty(ref this.notification, value as SpecialPageNotification);
            }
        }

        public GetSpecialPageViewModel()
        {
            this.okCommand = new DelegateCommand<string>(Ok);
            this.cancelCommand = new DelegateCommand<string>(Cancel);
        }

        private void Cancel(string obj)
        {
            if (this.notification != null)
            {
                this.notification.Confirmed = false;
                this.notification.FromNo = null;
                this.notification.ToNo = null;
            }

            this.FinishInteraction();
        }

        private void Ok(string obj)
        {
            if (this.notification != null)
            {
                this.notification.Confirmed = true;
            }
            this.FinishInteraction();
        }

        public ICommand OkCommand { get { return this.okCommand; } }
        public ICommand CancelCommand { get { return this.cancelCommand; } }
    }
}
