﻿using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.Edit.QA
{
    //[Export(typeof(TMAddNotification))]
    //[PartCreationPolicy(CreationPolicy.NonShared)]
    public class TMAddNotification : Confirmation
    {
        public TMAddNotification()
        {
            this.Users = new ObservableCollection<Account>();
            Trademark = null;
        }

        public TMAddNotification(TMItem trademark, IList<Account> users)
            : this()
        {
            Trademark = trademark;
            foreach (Account item in users)
            {
                this.Users.Add(item);
            }
        }

        public IList<Account> Users { get; private set; }

        public TMItem Trademark { get; set; }

        


    }

   
}
