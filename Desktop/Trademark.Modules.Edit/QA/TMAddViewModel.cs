﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.Edit.QA
{
    //[Export(typeof(TMAddViewModel))]
    //[PartCreationPolicy(CreationPolicy.NonShared)]
    public class TMAddViewModel : BindableBase, IInteractionRequestAware
    {
        private TMAddNotification notification;

        public Action FinishInteraction { get; set; }

        public TMAddViewModel()
        {
            this.AddCommand = new DelegateCommand(this.AcceptSelectedItem);
            this.CancelCommand = new DelegateCommand(this.CancelInteraction);
        }

        //[Import]
        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                SetProperty(ref this.notification, value as TMAddNotification);
            }
        }

        public ICommand AddCommand { get; private set; }

        public ICommand CancelCommand { get; private set; }

        public void AcceptSelectedItem()
        {
            if (this.notification != null)
            {
                this.notification.Trademark.UserId = this.SelectedValue;
                this.notification.Confirmed = true;
            }

            this.FinishInteraction();
        }


        public int? SelectedValue { get; set; }

        public void CancelInteraction()
        {
            if (this.notification != null)
            {
                this.notification.Trademark = null;
                this.notification.Confirmed = false;
            }

            this.FinishInteraction();
        }
    }
}
