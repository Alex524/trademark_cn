﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;

namespace Trademark.Modules.Edit.QA
{
    /// <summary>
    /// Interaction logic for QASummaryView.xaml
    /// </summary>
    [ViewExport(RegionName = RegionNames.MainRegion)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class QASummaryView : UserControl
    {
        public QASummaryView()
        {
            InitializeComponent();
        }

        [Import]
        IQASummaryViewModel model
        {
            set { this.DataContext = value as IQASummaryViewModel; }
        }
    }
}
