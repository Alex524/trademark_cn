﻿using Prism.Commands;
using Prism.Events;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Modules.Edit.QA;
using Trademark.Service.Contract;

namespace Trademark.Modules.Edit.Summary
{
    [Export(typeof(QASearchViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class QASearchViewModel : BindableBase
    {
        private readonly ILoginService LoginService;
        private readonly IEventAggregator EventAggregator;
        private ObservableCollection<TMItem> tmItems;
        private TMItem currentTM;
        private readonly ITrademarkFeedService TrademarkFeedService;
        private ICommand getCommand;
        private ICommand getSpecialPageCommand;
        private readonly ICommand addCommand;

        private readonly ICommand removeCommand;
        private readonly IPageFeedService PageFeedService;
        private bool refresh;

        public InteractionRequest<TMAddNotification> TMAddRequest { get; private set; }
        public InteractionRequest<SpecialPageNotification> SpecialPageRequest { get; private set; }
        public InteractionRequest<INotification> NotificationRequest { get; private set; }

        [ImportingConstructor]
        public QASearchViewModel(ILoginService loginService, IEventAggregator eventAggregator, ITrademarkFeedService trademarkFeedService, IPageFeedService pageFeedService)
        {
            this.LoginService = loginService;
            this.EventAggregator = eventAggregator;
            this.TrademarkFeedService = trademarkFeedService;
            this.PageFeedService = pageFeedService;
            this.getCommand = new DelegateCommand<TMStatus?>(Get);
            this.getSpecialPageCommand = new DelegateCommand<TMStatus?>(GetSpecialPage);
            this.addCommand = new DelegateCommand<string>(Add);
            this.removeCommand = new DelegateCommand<int?>(Remove);
            this.TMItems = new ObservableCollection<TMItem>();
            this.EventAggregator.GetEvent<TrademarkSaveEvent>().Subscribe((arg) => { Refresh(arg); });
            this.TMAddRequest = new InteractionRequest<TMAddNotification>();
            this.SpecialPageRequest = new InteractionRequest<SpecialPageNotification>();
            this.NotificationRequest = new InteractionRequest<INotification>();
        }

        private void Refresh(TMStatus? arg)
        {
            this.refresh = true;
            this.Search(arg);
        }

        private void GetSpecialPage(TMStatus? status)
        {
            this.refresh = false;
            if (!(status == TMStatus.QAPass))
                return;

            this.SpecialPageRequest.Raise(new SpecialPageNotification { Title = "Set Special Page No." }, c =>
            {
                if (c.Confirmed && c.FromNo.HasValue)
                {
                    TMItems.Clear();
                    CurrentTM = null;
                    var page = this.PageFeedService.GetSpecialPage(c.FromNo, c.ToNo);
                    if (null != page)
                    {
                        this.PageNo = page.Page;
                        this.PageId = page.Id;

                        var result = this.TrademarkFeedService.Search(new Service.Contract.SearchModel { GGQH = page.PubNo, GGRQ = page.PubDate, PageNo = page.Page });

                        if (result != null)
                        {
                            foreach (var item in result)
                                this.TMItems.Add(new TMItem(item));
                        }
                    }
                }
            });
        }

        private void Get(TMStatus? status)
        {
            this.refresh = false;
            //validate the Current page status and update it if all of parts of them have been finished;
            if (this.TMItems.Where(x => (x.Status | TMStatus.QAPass) != x.Status && (x.Status | TMStatus.QAReturn) != x.Status).Count() > 0)
            {
                this.NotificationRequest.Raise(new Notification { Content = "Exist un-checked trademark, please pass or reject it first!", Title = "Notification" });
                refresh = true;
            }
            else if (this.TMItems.Where(x => (x.Status | TMStatus.QAReturn) == x.Status).Count() > 0)
            {
                this.PageFeedService.UpdatePage(8);
            }
            else
            {
                this.PageFeedService.UpdatePage(16);
            }

            this.Search(status);

        }

        private void Remove(int? id)
        {
            this.TrademarkFeedService.Remove(id);
            this.Search(TMStatus.QAPass);

        }

        private void Add(string obj)
        {
            TMItem tm = new TMItem { GGQH = PageFeedService.GetCurrentPage().PubNo, GGRQ = PageFeedService.GetCurrentPage().PubDate, PageNo = PageFeedService.GetCurrentPage().Page };

            var users = LoginService.GetAllUsers();

            this.TMAddRequest.Raise(new TMAddNotification(tm, users) { Title = "Trademark Add" }, c =>
             {
                 if (c.Confirmed)
                 {
                     CurrentTM = new TMItem { UserId = c.Trademark.UserId, GGQH = c.Trademark.GGQH, GGRQ = c.Trademark.GGRQ, PageNo = c.Trademark.PageNo, Status = TMStatus.Add };
                     this.TMItems.Add(currentTM);
                 }
             });
        }

        private void Search(TMStatus? obj)
        {
            TMPage page = null;
            if (!(obj == TMStatus.QAPass || obj == TMStatus.QAReturn))
                return;

            TMItems.Clear();
            CurrentTM = null;

            page = this.PageFeedService.GetPage(refresh);
            if (null != page)
            {
                this.PageNo = page.Page;
                this.PageId = page.Id;
                var result = this.TrademarkFeedService.Search(new Service.Contract.SearchModel { GGQH = page.PubNo, GGRQ = page.PubDate, PageNo = page.Page });

                if (result != null)
                {
                    foreach (var item in result)
                        this.TMItems.Add(new TMItem(item));
                }
            }
        }




        private void UpdateCurrentPage()
        {
            PageFeedService.SetCurrentPage(new Service.Contract.TMPage { UserId = LoginService.GetAccount().Id, PubNo = this.GGQH, Page = this.PageNo, Id = this.PageId });
        }

        public ObservableCollection<TMItem> TMItems
        {
            get { return this.tmItems; }
            set
            {
                SetProperty(ref this.tmItems, value);
            }
        }

        private int? ggqh;
        public int? GGQH
        {
            get
            {
                return this.ggqh;
            }
            set
            {
                if (value != this.ggqh)
                {
                    SetProperty(ref this.ggqh, value);
                    UpdateCurrentPage();
                    //TrademarkFeedService.GetCurrentPage().GGQH = value;
                }
            }
        }

        private int? pageNo;

        public ICommand AddCommand { get { return this.addCommand; } }


        public ICommand GetCommand { get { return this.getCommand; } }
        public ICommand GetSpecialPageCommand { get { return this.getSpecialPageCommand; } }

        public ICommand RemoveCommand { get { return this.removeCommand; } }

        public TMItem CurrentTM
        {
            get
            {
                return currentTM;
            }

            set
            {
                if (SetProperty(ref currentTM, value))
                {
                    EventAggregator.GetEvent<TMQASelectedEvent>().Publish(
                        currentTM);
                }
            }
        }

        public int? PageNo
        {
            get
            {
                return pageNo;
            }

            set
            {
                SetProperty(ref this.pageNo, value);
                UpdateCurrentPage();
            }
        }

        public int _PageId;
        public int PageId
        {
            get
            {
                return _PageId;
            }
            private set
            {
                SetProperty(ref this._PageId, value); UpdateCurrentPage();
            }
        }
    }
}
