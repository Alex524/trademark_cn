﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Service.Contract;

namespace Trademark.Modules.Login.Services
{
    [Export(typeof(ILoginService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class LoginService : ILoginService
    {

        private IList<Account> accounts = null;
        private Account _account = null;
        public event EventHandler<AccountEventArgs> Updated = delegate { };

        public Account GetAccount()
        {
            return _account;
        }

        public IList<Account> GetAllUsers()
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IUserService>();
                var users = service.GetAllUsers();

                if (users != null)
                {
                    accounts = users.Select(user => new Account
                    {
                        Id = user.Id,
                        IsActive = user.IsActive,
                        Name = user.Name,
                        Password = user.Password,
                        DisplayName = user.DisplayName,
                        Role = (Role?)user.Role
                    }).ToList();
                }
            }
            return accounts;

        }

        public void Login(string name, string password)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IUserService>();
                var user = service.Login(name, password);
                if (user != null)
                {
                    _account = new Account
                    {
                        Id = user.Id,
                        IsActive = user.IsActive,
                        Name = user.Name,
                        Password = user.Password,
                        DisplayName = user.DisplayName,
                        Role = (Role?)user.Role
                    };
                    this.Updated(this, new AccountEventArgs(_account));
                }
            }
        }

        public void Save(Account account)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<IUserService>();
                service.Save(new User
                {
                    Id = account.Id,
                    Name = account.Name,
                    DisplayName = account.DisplayName,
                    Password = account.Password,
                    IsActive = account.IsActive,
                    Role = (int?)account.Role
                });
            }
        }
    }
}
