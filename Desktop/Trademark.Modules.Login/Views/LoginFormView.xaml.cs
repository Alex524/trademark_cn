﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;

namespace Trademark.Modules.Login.Views
{
    /// <summary>
    /// Interaction logic for LoginForm.xaml
    /// </summary>
    [ViewExport("LoginFormView")]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class LoginFormView : UserControl
    {
        public LoginFormView()
        {
            InitializeComponent();
        }

        [Import]
        LoginFormViewModel ViewModel
        {
            set { this.DataContext = value; }
        }
    }
}
