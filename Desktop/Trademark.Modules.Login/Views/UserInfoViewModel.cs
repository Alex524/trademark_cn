﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.Login.Views
{
    [Export(typeof(UserInfoViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class UserInfoViewModel : BindableBase
    {
        private readonly DelegateCommand<string> changeUserCommand;
        private readonly IEventAggregator EventAggregator;
        private readonly ILoginService LoginService;
        [ImportingConstructor]
        public UserInfoViewModel(IEventAggregator eventAggregator, ILoginService loginService)
        {
            this.EventAggregator = eventAggregator;
            this.LoginService = loginService;

            changeUserCommand = new DelegateCommand<string>(ChangeUser);

            LoginService.Updated += LoginService_Updated;
        }

        private void ChangeUser(string obj)
        {
            this.EventAggregator.GetEvent<ShowLoginEvent>().Publish(null);
        }

        private void LoginService_Updated(object sender, Infrastructure.Models.AccountEventArgs e)
        {
            if (e.Account != null)
                this.DisplayName = !string.IsNullOrEmpty(e.Account.DisplayName) ? e.Account.DisplayName : e.Account.Name;
        }

        public string _displayName;
        public string DisplayName
        {
            get { return this._displayName; }
            private set
            {
                SetProperty(ref this._displayName, value);
            }
        }

        public ICommand ChangeUserCommand
        {
            get
            {
                return this.changeUserCommand;
            }
        }
    }
}
