﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.Login.Views
{
    [Export(typeof(LoginFormViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class LoginFormViewModel : BindableBase
    {
        private string name;
        private string password;
        private ILoginService loginService;
        private ICommand loginCommand { get; set; }
        private ICommand resetCommand { get; set; }

        [ImportingConstructor]
        public LoginFormViewModel(ILoginService loginService)
        {
            this.loginService = loginService;
            this.loginCommand = new DelegateCommand<PasswordBox>(this.Login);
            resetCommand = new DelegateCommand<object>(this.Reset);
        }

        private void Reset(object obj)
        {
            this.Name = null;
            this.Password = null;
        }

        private void Login(PasswordBox box)
        {
            loginService.Login(this.Name, box.Password);
        }

        public string Name
        {
            get { return this.name; }
            set
            {
                SetProperty(ref this.name, value);
            }
        }

        public string Password
        {
            get { return this.password; }
            set
            {
                SetProperty(ref this.password, value);
            }
        }

        public ICommand LoginCommand { get { return this.loginCommand; } }
        public ICommand ResetCommand { get { return this.resetCommand; } }
    }
}
