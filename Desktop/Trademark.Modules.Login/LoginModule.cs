﻿using Prism.Events;
using Prism.Mef.Modularity;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Modules.Login.Views;

namespace Trademark.Modules.Login
{
    [ModuleExport(typeof(LoginModule))]
    public class LoginModule : IModule
    {
        private IRegionManager RegionManager { get; set; }
        private IEventAggregator EventAggregator { get; set; }
        [ImportingConstructor]
        public LoginModule(IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            this.RegionManager = regionManager;
            this.EventAggregator = eventAggregator;
        }
        public void Initialize()
        {
            this.EventAggregator.GetEvent<ShowLoginEvent>().Subscribe(ShowLogin);
        }

        private void ShowLogin(string Name)
        {
            this.RegionManager.RequestNavigate(RegionNames.SecondaryRegion, new Uri("/LoginFormView", UriKind.Relative));
        }
    }
}
