﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Media.Imaging;

namespace LH.Helper
{
    public static class FileHelper
    {
        public static Byte[] ImageToBytes(this BitmapSource imageSource)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(imageSource));
                encoder.Save(memoryStream);
                return memoryStream.GetBuffer();

            }
            //Byte[] buffer = null;
            //if (stream != null && stream.Length > 0)
            //{
            //    using (BinaryReader br = new BinaryReader(stream))
            //    {
            //        buffer = br.ReadBytes((Int32)stream.Length);
            //    }
            //}

            //return buffer;
        }

        public static string RemoveEnds(this string str, string reg)
        {
            return Regex.Replace(str, reg, "", RegexOptions.IgnoreCase);
        }

        public static T File2Obj<T>(string fileStr)
        {
            using (StringReader reader = new StringReader(fileStr))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }

        public static T BytesToObject<T>(byte[] bytes)
        {
            Stream stream = null;
            try
            {
                stream = new MemoryStream(bytes);
                XmlReaderSettings xrs = new XmlReaderSettings();
                xrs.DtdProcessing = DtdProcessing.Ignore;
                xrs.ValidationType = ValidationType.None;

                XmlReader xr = XmlReader.Create(stream, xrs);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(xr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
        }

        public static DirectoryInfo[] GetSubDirectories(string path, string pattern)
        {
            DirectoryInfo info = new DirectoryInfo(path);
            if (!info.Exists)
            {
                info.Create();
            }
            if (pattern != null)
            {
                return info.GetDirectories(pattern, SearchOption.AllDirectories);
            }
            return info.GetDirectories();
        }

        public static IList<FileInfo> GetFolderFiles(string path, string pattern, SearchOption option)
        {
            DirectoryInfo info = new DirectoryInfo(path);
            if (!info.Exists)
            {
                info.Create();
            }
            if (pattern != null)
            {
                return info.GetFiles(pattern, option);
            }
            return info.GetFiles("*.*", option);
        }

        public static void MoveTo(this FileInfo fileInfo, DirectoryInfo directoryInfo, string fileName)
        {
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            string str = Path.Combine(directoryInfo.FullName, fileName);
            FileInfo info2 = new FileInfo(str);
            if (info2.Exists)
            {
                info2.Delete();
            }
            fileInfo.MoveTo(str);
        }


        public static IList<string> ReadFileToList(this FileInfo fileInfo)
        {
            FileStream stream = null;
            StreamReader reader = null;
            string line = null;
            IList<string> result = new List<string>();
            using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            //using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                using (reader = OpenStream(stream, Encoding.Default))
                {
                    while (!reader.EndOfStream)
                    {
                        line = reader.ReadLine();
                        if (null != line && !string.IsNullOrEmpty(line.Trim()))
                            result.Add(line.Trim());
                    }
                }
            }
            return result;
        }

        public static string ReadText(this FileInfo fileInfo)
        {
            FileStream stream = null;
            StreamReader reader = null;
            using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            //using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                using (reader = OpenStream(stream, Encoding.Default))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static void MoveTo(this FileInfo fileInfo, string directoryName, string fileName)
        {
            DirectoryInfo info = new DirectoryInfo(directoryName);
            if (!info.Exists)
            {
                info.Create();
            }
            string str = Path.Combine(info.FullName, fileName);
            FileInfo info2 = new FileInfo(str);
            if (info2.Exists)
            {
                info2.Delete();
            }
            fileInfo.MoveTo(str);
        }

        public static XmlDocument Obj2Xml(object obj, XmlSerializerNamespaces namespaces)
        {
            XmlDocument document = null;
            string xml = null;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8))
                    {
                        new XmlSerializer(obj.GetType()).Serialize(writer, obj, namespaces);
                        if (stream.Position > 0)
                            stream.Position = 0;
                        document = new XmlDocument();
                        document.Load(stream);
                        //RemoveSpaceNode(document.ChildNodes);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return document;
        }

        public static XDocument Obj2XDocument(object obj, XmlSerializerNamespaces namespaces)
        {
            XDocument document = null;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8))
                    {
                        new XmlSerializer(obj.GetType()).Serialize(writer, obj, namespaces);
                        if (stream.Position > 0)
                            stream.Position = 0;

                        document = XDocument.Load(stream);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return document;
        }

        public static void RemoveSpaceNode(this XDocument xdoc)
        {
            RemoveEmptyNodes(xdoc.Elements());
        }

        private static void RemoveEmptyNodes(IEnumerable<XElement> elements)
        {
            //fist recur the method to the leaf node, then start to remove the empty nodes.
            foreach (XElement item in elements)
            {
                if (item.HasElements)
                    RemoveEmptyNodes(item.Elements());
            }

            IEnumerable<XElement> empties = elements.Where(x => !x.HasAttributes && !x.HasElements && string.IsNullOrEmpty(x.Value));
            if (empties.Count() > 0)
                empties.Remove();
        }

        public static string Obj2Str(object obj, XmlSerializerNamespaces namespaces)
        {
            string xml = null;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8))
                    {
                        new XmlSerializer(obj.GetType()).Serialize(writer, obj, namespaces);
                        xml = Encoding.UTF8.GetString(stream.ToArray()).TrimStart(' ');
                        if (!xml.StartsWith("<"))
                        {
                            xml = xml.Substring(1);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return xml;
        }


        public static string ReadFile(this FileInfo fileInfo, Encoding encoding)
        {
            FileStream stream = null;
            StreamReader reader = null;
            string str = null;
            using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                using (reader = new StreamReader(stream, encoding))
                {
                    str = reader.ReadToEnd();
                }
            }
            return str;
        }

        public static string ReadFile(this FileInfo fileInfo)
        {
            FileStream stream = null;
            StreamReader reader = null;
            string str = null;
            using (stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                using (reader = new StreamReader(stream))
                {
                    str = reader.ReadToEnd();
                }
            }
            return str;
        }

        public static T File2Obj<T>(this FileInfo fileInfo)
        {
            using (FileStream stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                XmlReaderSettings settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Ignore,
                    ValidationType = ValidationType.None
                };
                XmlReader reader = XmlReader.Create(stream, settings);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }

        public static XmlDocument ReadXml(this FileInfo fileInfo, Encoding encoding)
        {
            XmlDocument document = null;
            using (FileStream stream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read))
            {
                XmlReaderSettings settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Ignore,
                    ValidationType = ValidationType.None
                };
                XmlReader reader = XmlReader.Create(stream, settings);
                document = new XmlDocument();
                document.Load(reader);
            }
            return document;
        }

        public static XmlDocument ReadXml(this string fileStr, Encoding encoding)
        {
            XmlDocument document = null;
            using (StringReader sr = new StringReader(fileStr))
            {
                XmlReaderSettings settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Ignore,
                    ValidationType = ValidationType.None
                };
                XmlReader reader = XmlReader.Create(sr, settings);
                document = new XmlDocument();
                document.Load(reader);
            }
            return document;
        }

        public static XDocument ReadLinqXml(this string fileStr, Encoding encoding)
        {
            XDocument document = null;
            using (StringReader sr = new StringReader(fileStr))
            {
                XmlReaderSettings settings = new XmlReaderSettings
                {
                    DtdProcessing = DtdProcessing.Ignore,
                    ValidationType = ValidationType.None
                };
                XmlReader reader = XmlReader.Create(sr, settings);
                document = XDocument.Load(reader);
                //document.Load(reader);
            }
            return document;
        }

        public static void RemoveSpaceNode(XmlNodeList XNL)
        {
            foreach (XmlNode node in XNL)
            {
                if ((((node.GetType() != typeof(XmlDeclaration)) && (node.GetType() != typeof(XmlDocumentType))) && (node.ChildNodes.Count == 0)) && (((node.InnerText == null) || (node.InnerText.Trim().Length == 0)) && ((node.Attributes == null) || (node.Attributes.Count == 0))))
                {
                    XmlNode parentNode = node.ParentNode;
                    node.ParentNode.RemoveChild(node);
                    continue;
                }
                RemoveSpaceNode(node.ChildNodes);
            }
        }

        public static void WriteFile(string fullName, string txt)
        {
            FileInfo info = new FileInfo(fullName);
            if (!info.Directory.Exists)
                info.Directory.Create();
            if (info.Exists)
                info.Delete();
            using (FileStream stream = new FileStream(fullName, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    writer.Flush();
                    //writer.BaseStream.Seek(0L, SeekOrigin.Begin);
                    writer.Write(txt);
                    writer.Flush();
                }
            }
        }

        public static void WriteToText(string fullName, string txt)
        {
            FileInfo file = new FileInfo(fullName);

            if (!file.Directory.Exists)
            {
                file.Directory.Create();
                file.Directory.Attributes = FileAttributes.Archive;
            }

            if (!file.Exists)
            {
                using (FileStream fileStream = new FileStream(file.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    fileStream.Flush();
                }
            }

            using (StreamWriter writer = new StreamWriter(file.FullName, true, Encoding.GetEncoding("UTF-8")))
            {
                writer.WriteLine(txt);
                writer.Flush();
                writer.Close();
            }
        }

        public static void WriteBytes(string fullName, byte[] content)
        {
            FileInfo info = new FileInfo(fullName);

            if (!info.Directory.Exists)
                info.Directory.Create();
            if (info.Exists)
                info.Delete();

            File.WriteAllBytes(fullName, content);
        }

        public static void SaveXml(this XmlDocument xmlDoc, string fullFileName)
        {
            FileInfo info = new FileInfo(fullFileName);
            if (!info.Directory.Exists)
            {
                info.Directory.Create();
            }
            if (info.Exists)
            {
                info.Delete();
            }
            xmlDoc.Save(fullFileName);
        }

        public static void SaveXml(this XDocument xmlDoc, string fullFileName)
        {
            FileInfo info = new FileInfo(fullFileName);
            if (!info.Directory.Exists)
            {
                info.Directory.Create();
            }
            if (info.Exists)
            {
                info.Delete();
            }

            xmlDoc.Save(fullFileName);
        }

        public static void SaveBytes(this byte[] bytes, string fullFileName)
        {
            FileInfo fileInfo = new FileInfo(fullFileName);
            if (!fileInfo.Directory.Exists)
                fileInfo.Directory.Create();

            if (fileInfo.Exists)
                fileInfo.Delete();

            File.WriteAllBytes(fullFileName, bytes);
        }


        public static StreamReader OpenStream(Stream fs, Encoding defaultEncoding)
        {
            if (fs == null)
                throw new ArgumentNullException("fs");

            if (fs.Length >= 2)
            {
                // the autodetection of StreamReader is not capable of detecting the difference  
                // between ISO-8859-1 and UTF-8 without BOM.  
                int firstByte = fs.ReadByte();
                int secondByte = fs.ReadByte();
                switch ((firstByte << 8) | secondByte)
                {
                    case 0x0000: // either UTF-32 Big Endian or a binary file; use StreamReader  
                    case 0xfffe: // Unicode BOM (UTF-16 LE or UTF-32 LE)  
                    case 0xfeff: // UTF-16 BE BOM  
                    case 0xefbb: // start of UTF-8 BOM  
                        // StreamReader autodetection works  
                        fs.Position = 0;
                        return new StreamReader(fs);
                    default:
                        return AutoDetect(fs, (byte)firstByte, (byte)secondByte, defaultEncoding);
                }
            }
            else
            {
                if (defaultEncoding != null)
                {
                    return new StreamReader(fs, defaultEncoding);
                }
                else
                {
                    return new StreamReader(fs);
                }
            }
        }

        static StreamReader AutoDetect(Stream fs, byte firstByte, byte secondByte, Encoding defaultEncoding)
        {
            int max = (int)Math.Min(fs.Length, 500000); // look at max. 500 KB  
            const int ASCII = 0;
            const int Error = 1;
            const int UTF8 = 2;
            const int UTF8Sequence = 3;
            int state = ASCII;
            int sequenceLength = 0;
            byte b;
            for (int i = 0; i < max; i++)
            {
                if (i == 0)
                {
                    b = firstByte;
                }
                else if (i == 1)
                {
                    b = secondByte;
                }
                else
                {
                    b = (byte)fs.ReadByte();
                }
                if (b < 0x80)
                {
                    // normal ASCII character  
                    if (state == UTF8Sequence)
                    {
                        state = Error;
                        break;
                    }
                }
                else if (b < 0xc0)
                {
                    // 10xxxxxx : continues UTF8 byte sequence  
                    if (state == UTF8Sequence)
                    {
                        --sequenceLength;
                        if (sequenceLength < 0)
                        {
                            state = Error;
                            break;
                        }
                        else if (sequenceLength == 0)
                        {
                            state = UTF8;
                        }
                    }
                    else
                    {
                        state = Error;
                        break;
                    }
                }
                else if (b >= 0xc2 && b < 0xf5)
                {
                    // beginning of byte sequence  
                    if (state == UTF8 || state == ASCII)
                    {
                        state = UTF8Sequence;
                        if (b < 0xe0)
                        {
                            sequenceLength = 1; // one more byte following  
                        }
                        else if (b < 0xf0)
                        {
                            sequenceLength = 2; // two more bytes following  
                        }
                        else
                        {
                            sequenceLength = 3; // three more bytes following  
                        }
                    }
                    else
                    {
                        state = Error;
                        break;
                    }
                }
                else
                {
                    // 0xc0, 0xc1, 0xf5 to 0xff are invalid in UTF-8 (see RFC 3629)  
                    state = Error;
                    break;
                }
            }
            fs.Position = 0;
            switch (state)
            {
                case ASCII:
                case Error:
                    // when the file seems to be ASCII or non-UTF8,  
                    // we read it using the user-specified encoding so it is saved again  
                    // using that encoding.  
                    //if (IsUnicode(defaultEncoding))
                    //{
                    //    // the file is not Unicode, so don't read it using Unicode even if the  
                    //    // user has choosen Unicode as the default encoding.  

                    //    // If we don't do this, SD will end up always adding a Byte Order Mark  
                    //    // to ASCII files.  
                    //    defaultEncoding = Encoding.Default; // use system encoding instead  
                    //}
                    return new StreamReader(fs, defaultEncoding);
                default:
                    return new StreamReader(fs);
            }
        }

        //public static void WriteFile(string fullName,string strErrMessage)
        //{
        //    FileInfo file = new FileInfo(fullName);

        //    if (!file.Directory.Exists)
        //    {
        //        file.Directory.Create();
        //        file.Directory.Attributes = FileAttributes.Archive;
        //    }

        //    if (!file.Exists)
        //    {
        //        using (FileStream fileStream = new FileStream(file.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite))
        //        {
        //            fileStream.Flush();
        //        }
        //    }

        //    try
        //    {
        //        using (StreamWriter writer = new StreamWriter(file.FullName, true, Encoding.GetEncoding("UTF-8")))
        //        {
        //            writer.WriteLine(string.Concat(new object[] { DateTime.Now.ToString(), Convert.ToChar(9), Convert.ToChar(9), strErrMessage, "\n\t" }));
        //            writer.Flush();
        //            writer.Close();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    finally
        //    {
        //    }
        //}
    }
}
