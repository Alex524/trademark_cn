﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Xsl;
//using MS.Internal.Xml;

namespace LH.Helper
{
    public static class ExcelExport
    {
        public static string TransformXMLDocumentToExcel(string xmlStr, string xslFileName, string excelFileName)
        {
            string strXSLFullFilePath = null;
            System.Xml.XPath.XPathDocument objXPthDoc;
            XslCompiledTransform objXslTran;
            
            try
            {
                using (StringReader strreader = new StringReader(xmlStr))
                {
                    using (XmlReader xmlreader = new XmlTextReader(strreader))
                    {
                        objXPthDoc = new System.Xml.XPath.XPathDocument(xmlreader);
                        FileInfo fi = new FileInfo(excelFileName);
                        if (!fi.Directory.Exists)
                            fi.Directory.Create();
                        using (FileStream fs = new FileStream(excelFileName, FileMode.Create))
                        {
                            using (XmlTextWriter objXMLTxtWrtr = new XmlTextWriter(fs, System.Text.Encoding.Unicode))
                            {
                                objXslTran = new XslCompiledTransform();
                                strXSLFullFilePath = Path.Combine("XSLStyleSheet", xslFileName);
                                objXslTran.Load(strXSLFullFilePath);
                                objXslTran.Transform(objXPthDoc, objXMLTxtWrtr);
                            }
                        }
                    }
                }
                return excelFileName;
            }
            catch (Exception exptn)
            {
                throw exptn;
            }
        }
    }
}
