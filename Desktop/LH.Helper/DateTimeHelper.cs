﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace LH.Helper
{
    public static class DateTimeHelper
    {

        public static string Str2DateStr(this string str, CultureInfo culture)
        {
            DateTime datetime = DateTime.MinValue;
            if (str != null && DateTime.TryParse(str.Trim(), culture, System.Globalization.DateTimeStyles.None, out datetime))
                return datetime.ToString("yyyy-MM-dd");
            return "";
        }

        public static DateTime? Str2DateTime(this string str, CultureInfo culture)
        {
            DateTime datetime = DateTime.MinValue;

            if (str.Trim().Length == 8)
                str = str.Insert(6, "-").Insert(4,"-");

            if (str != null && DateTime.TryParse(str.Trim(), culture, System.Globalization.DateTimeStyles.None, out datetime))
                return datetime;
            return null;
        }
    }
}
