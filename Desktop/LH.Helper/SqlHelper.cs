﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace LH.Helper
{
    public class SqlHelper
    {
        public static string CommitTable(SqlConnection cnn, DataTable dtValue, string tblName, string keyColName)
        {
            if ((dtValue != null) && (dtValue.Rows.Count != 0))
            {
                SqlCommandBuilder builder = null;
                SqlDataAdapter adapter = GetSelectCommand(cnn, dtValue, tblName);
                builder = new SqlCommandBuilder(adapter);
                if (!string.IsNullOrEmpty(keyColName))
                {
                    adapter.InsertCommand = builder.GetInsertCommand().Clone();
                    adapter.InsertCommand.CommandText = string.Format("{0};SELECT @@IDENTITY as [{1}]", adapter.InsertCommand.CommandText, keyColName);
                    adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;
                }
                try
                {
                    adapter.Update(dtValue);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
            return null;
        }

        public static SqlConnection Connect(string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public static DataSet ExecuteSP(string cnnStr, string sp_name, params SqlParameter[] parms)
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = Connect(cnnStr))
            {
                using (SqlCommand command = new SqlCommand(sp_name, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    if ((parms != null) && (parms.Length > 0))
                    {
                        SqlParameter parameter = null;
                        foreach (SqlParameter parameter2 in parms)
                        {
                            command.Parameters.Add(parameter = new SqlParameter(parameter2.ParameterName, parameter2.SqlDbType));
                            parameter.Direction = parameter2.Direction;
                            parameter.Value = parameter2.Value;
                        }
                    }
                    new SqlDataAdapter(command).Fill(dataSet);
                }
            }
            return dataSet;
        }

        private static SqlDataAdapter GetSelectCommand(SqlConnection cnn, DataTable dtValue, string tblName)
        {
            return new SqlDataAdapter(string.Format("SELECT * FROM {0}", tblName), cnn);
        }

        public static DataTable GetTable(SqlConnection cnn, string sql)
        {
            DataTable dataTable = new DataTable();
            using (SqlDataAdapter adapter = new SqlDataAdapter(sql, cnn))
            {
                adapter.Fill(dataTable);
            }
            return dataTable;
        }

        public static DataTable GetTable(string cnnStr, string sql)
        {
            DataTable dataTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(cnnStr))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(sql, connection))
                {
                    adapter.Fill(dataTable);
                }
            }
            return dataTable;
        }

        public static DataSet GetTables(SqlConnection cnn, string sql)
        {
            DataSet dataSet = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, cnn);
            try
            {
                adapter.Fill(dataSet);
            }
            catch (Exception)
            {
                return new DataSet();
            }
            finally
            {
                if (adapter != null)
                {
                    adapter.Dispose();
                }
            }
            return dataSet;
        }

        public static void ExecNoneQuery(string cnnStr, string sql)
        {
            using (SqlConnection connection = new SqlConnection(cnnStr))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static DataSet GetTables(string cnnStr, string sql)
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = new SqlConnection(cnnStr))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(sql, connection))
                {
                    try
                    {
                        adapter.Fill(dataSet);
                    }
                    catch (Exception)
                    {
                        return new DataSet();
                    }
                }
            }
            return dataSet;
        }

    }
}
