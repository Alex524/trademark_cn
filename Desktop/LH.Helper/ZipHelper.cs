﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LH.Helper
{
    public static class ZipHelper
    {
        public static void Zip(string SourceDir, string ToFullName)
        {
            ProcessStartInfo p = new ProcessStartInfo();
            p.FileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "lib", "7z.exe");

            FileInfo fi = new FileInfo(ToFullName);
            if (!fi.Directory.Exists)
                fi.Directory.Create();


            // 2
            // Use 7-zip
            // specify a=archive and -tgzip=gzip
            // and then target file in quotes followed by source file in quotes
            //
            p.Arguments = "a -tzip \"" + ToFullName + "\" \"" + SourceDir + "\" -mx=9";
            p.WindowStyle = ProcessWindowStyle.Hidden;

            // 3.
            // Start process and wait for it to exit
            //
            Process x = Process.Start(p);
            x.WaitForExit();
        }

        public static void UnZip(string zipfile, string toFullDir)
        {
            ProcessStartInfo p = new ProcessStartInfo();
            p.FileName = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "lib", "7z.exe");

            DirectoryInfo fi = new DirectoryInfo(toFullDir);
            if (fi.Exists)
                fi.Delete(true);

            p.Arguments = "x -tzip \"" + zipfile + "\" -o\"" + toFullDir + "\" -r";
            p.WindowStyle = ProcessWindowStyle.Hidden;

            Process x = Process.Start(p);
            x.WaitForExit();
        }
    }
}
