﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;

namespace LH.Helper
{
    public static class HtmlHelper
    {
        private const string BRPATTERN = @"<br>";
        private const string SPACESPATTERN = @"([ ]|[ \t]){2,}";
        private const string METAPATTERN = @"<meta ([^>]*)>";
        private const string LINKPATTERN = @"<link ([^>]*)>";
        private const string INPUTPATTERN = @"<input ([^>]*)>";
        private const string IMGPATTERN = @"<img ([^>]*)>";
        private const string BODYPATTERN = @"<body.*</body>";

        public static string FormatHtml(this string inputHtml)
        {
            //inputHtml = Regex.Replace(inputHtml, @"(<[\s\S]+?>)", delegate(Match match)
            //{
            //    return match.Value.ToLower();
            //});
          
          
            inputHtml = inputHtml.Replace("&nbsp;", "").Replace("\r\n", "").Replace("\n", "").Replace("<tbody>", "").Replace("</tbody>", "").Replace("&", "&amp;").Replace("&amp;amp;","&amp;");
            inputHtml = Regex.Replace(inputHtml, SPACESPATTERN, " ");
            inputHtml = Regex.Replace(inputHtml, @"<!([^>]*)>", "");
            inputHtml = Regex.Replace(inputHtml, "-{2,}", "--");
            inputHtml = Regex.Replace(inputHtml, BRPATTERN, RegexMatch, RegexOptions.IgnoreCase);
            inputHtml = Regex.Replace(inputHtml, INPUTPATTERN, "", RegexOptions.IgnoreCase);
            //inputHtml = Regex.Replace(inputHtml, @"<div [\s\S]+?</div>", "", RegexOptions.IgnoreCase);
            inputHtml = Regex.Replace(inputHtml, IMGPATTERN, "", RegexOptions.IgnoreCase);
            inputHtml = Regex.Replace(inputHtml, @"<a [\s\S]+?>([\S\s]+?)</a>", delegate(Match match)
            {
                return match.Groups[1].Value;
            }, RegexOptions.IgnoreCase);
            inputHtml = inputHtml.Replace("#", "");//.Replace("</div>","");

            inputHtml = Regex.Match(inputHtml, BODYPATTERN, RegexOptions.IgnoreCase).Value;

            inputHtml = FixInvalidChars(inputHtml);

            return inputHtml;
        }

        public static string GetBodyFromHtml(this string inputHtml)
        {
            inputHtml = inputHtml.Replace("&nbsp;", "").Replace("\r\n", "").Replace("\n", "").Replace("<br/>", "").Replace("0x1c", "").Replace("0x1d", "").Replace("div", "DIV");

            inputHtml = FixInvalidChars(inputHtml);
            //inputHtml = Regex.Replace(inputHtml, IMGPATTERN, "", RegexOptions.IgnoreCase);
            return Regex.Match(inputHtml, BODYPATTERN, RegexOptions.IgnoreCase).Value;
        }

        public static string GetBodyFromHtmlOnly(this string inputHtml)
        {
            //inputHtml = inputHtml.Replace("&nbsp;", "").Replace("\r\n", "").Replace("\n", "").Replace("<br/>", "");
            return Regex.Match(inputHtml, BODYPATTERN, RegexOptions.Multiline).Value;
        }

        private static string RegexMatch(Match match)
        {
            if (!match.Value.Contains("/>"))
                return match.Value.Insert(match.Value.Length - 1, "/");
            else
                return match.Value;
        }

        public static byte[] Request_Post(string serviceUrl, string accept, string parameters)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serviceUrl);
            request.Method = "POST";
            request.Accept = accept;
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] b = System.Text.Encoding.UTF8.GetBytes(parameters);
            request.ContentLength = b.Length;
            request.ServicePoint.Expect100Continue = false;
            request.ServicePoint.ConnectionLimit = 100;

            Stream requestStream = null;
            HttpWebResponse response = null;
            Stream responseStream = null;
            StreamReader responseStreamReader = null;
            MemoryStream MS = null;
            byte[] resultbytes = null;
            try
            {
                requestStream = request.GetRequestStream();
                requestStream.Write(b, 0, b.Length);

                response = (HttpWebResponse)request.GetResponse();
                responseStream = response.GetResponseStream();

                byte[] bytes = new byte[1024];
                int size = responseStream.Read(bytes, 0, bytes.Length);
                MS = new MemoryStream();
                while (size > 0)
                {
                    MS.Write(bytes, 0, size);
                    size = responseStream.Read(bytes, 0, bytes.Length);
                }
                resultbytes = MS.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (requestStream != null) requestStream.Close();
                if (MS != null) MS.Close();
                if (responseStreamReader != null) responseStreamReader.Close();
                if (responseStream != null) responseStream.Close();
                if (response != null) response.Close();
            }
            return resultbytes;
        }

        public static byte[] Request_Get(string serviceUrl, string accept)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(serviceUrl);
            request.Method = "GET";
            request.Accept = accept;
            request.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse response = null;
            Stream responseStream = null;
            StreamReader responseStreamReader = null;
            MemoryStream MS = null;
            byte[] resultbytes = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                responseStream = response.GetResponseStream();

                byte[] bytes = new byte[1024];
                int size = responseStream.Read(bytes, 0, bytes.Length);
                MS = new MemoryStream();
                while (size > 0)
                {
                    MS.Write(bytes, 0, size);
                    size = responseStream.Read(bytes, 0, bytes.Length);
                }
                resultbytes = MS.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (MS != null) MS.Close();
                if (responseStreamReader != null) responseStreamReader.Close();
                if (responseStream != null) responseStream.Close();
                if (response != null) response.Close();
            }
            return resultbytes;
        }

        public struct Accept
        {
            public const string Exchange = "application/exchange+xml";
            public const string Fulltext = "application/fulltext+xml";
            public const string Ops = "application/ops+xml";
            public const string Png = "image/png";
            public const string Tiff = "application/tiff";
            public const string Pdf = "application/pdf";
        }

        public struct ImageType
        {
            public const string Pdf = "pdf";
            public const string Tiff = "tiff";
            public const string Png = "png";
        }

        static Regex RegexInvalidXmlChars = new Regex("[\u0000-\u0008\u000B\u000C\u000E-\u001F\uD800-\uDFFF\uFFFE\uFFFF]", RegexOptions.Compiled);
        static string FixInvalidChars(string txt, string replacement)
        {
            return RegexInvalidXmlChars.Replace(txt, replacement);
        }

        /// <summary>
        /// 替换不合法的字符
        ///http://www.bobopo.com/article/code/xml_invalid_character.htm
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string FixInvalidChars(string txt)
        {
            return FixInvalidChars(txt, "");
        }

    }
}
