﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Media.Imaging;

namespace LH.Helper
{
    public class ImageHelper
    {
        public static void SaveImage(string fullFileName, byte[] images)
        {
            FileInfo info = new FileInfo(fullFileName);
            if(!info.Directory.Exists)
                info.Directory.Create();

            if (info.Exists)
                info.Delete();

            using (FileStream file = new FileStream(fullFileName, FileMode.Create))
            {
                using (BinaryWriter writer = new BinaryWriter(file))
                {
                    writer.Write(images);
                }
            }
        }

        
    }
}
