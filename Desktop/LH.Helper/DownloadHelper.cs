﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace LH.Helper
{
    public static class DownloadHelper
    {
        public static byte[] RequestGet(string requrl, ICredentials credential, string accept)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(requrl));
            request.Method = "GET";
            request.Accept = accept;
            if (credential != null)
                request.Credentials = credential;
            request.ContentType = "application/x-www-form-urlencoded";

            HttpWebResponse response = null;
            Stream responseStream = null;
            StreamReader responseStreamReader = null;
            MemoryStream MS = null;
            byte[] resultbytes = null;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                responseStream = response.GetResponseStream();

                byte[] bytes = new byte[64*1024];
                int size;
                MS = new MemoryStream();
                while( (size = responseStream.Read(bytes, 0, bytes.Length))> 0)
                {
                    MS.Write(bytes, 0, size);
                }
                resultbytes = MS.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {   
                if (MS != null) MS.Close();
                if (responseStreamReader != null) responseStreamReader.Close();
                if (responseStream != null) responseStream.Close();
                if (response != null) response.Close();
            }
            return resultbytes;
        }
    }
}
