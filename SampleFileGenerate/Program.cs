﻿using LH.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Service.Contract;
using Trademark.Service.Implement.Repositories;

namespace SampleFileGenerate
{
    class Program
    {
        static TrademarkRepository Repository = new TrademarkRepository();
        static void Main(string[] args)
        {
            GetTMTrans();
        }
        public static void Run()
        {
            IList<TMElement> list = Repository.SampleGenerate();

            /* 
            APNR:	8792888
            APDA:	20101029
            CLAS:	30
            DESC:	Rice flour; cereal product; edible starch; beans
            TMNM:   湖头师傅
            TMTY:	W
            OWNN:	Xiamen Juzhen Trading Co., Ltd. 
            OWNA:	6F 501#, Xinglong Road, Huli District, Xiamen, Fujian
            OWNC:	CN
            CORN:	Quanzhou Fengze District Xinhua Trademark Agency Co., Ltd.
            */
            //string tms = null;
            foreach (var item in list)
            {
                string tmstr = null;

                //FileHelper.WriteToText("C:\\User\\Alex\\Desktop\\TM1535\\table.txt", string.Format("{1}{0}{2}{0}{3}{0}{4}{0}{5}{0}{6}{0}{7}{0}{8}{0}{9}{0}{10}{0}{11}{0}{12}", Convert.ToChar(9),
                //    item.Id, item.PubNumber, item.PageNo, item.NameCN, item.NameEN, item.ApplicantCN, item.ApplicantEN, item.AddressCN, item.AddressEN, item.AgentCN, item.AgentEN, item.OWNC));


                tmstr += string.Format("APNR:{1}{0}", item.PubNumber, Convert.ToChar(9));
                tmstr += "\r\n";
                tmstr += string.Format("APDA:{1}{0}", item.ApplicantDate.HasValue ? item.ApplicantDate.Value.ToString("yyyyMMdd") : null, Convert.ToChar(9));
                tmstr += "\r\n";
                foreach (var category in item.Categories.OrderBy(x => x.CategoryNumber))
                {
                    tmstr += string.Format("CLAS:{1}{0}", category.CategoryNumber, Convert.ToChar(9));
                    tmstr += "\r\n";
                    tmstr += string.Format("DESC:{1}{0}", category.DescriptionEN, Convert.ToChar(9));
                    tmstr += "\r\n";

                    //FileHelper.WriteToText("C:\\User\\Alex\\Desktop\\TM1535\\services.txt", string.Format("{1}{0}{2}{0}{3}{0}{4}", Convert.ToChar(9), item.Id, category.CategoryNumber, category.Description, category.DescriptionEN));
                }
                tmstr += string.Format("TMNM:{1}{0}", item.NameCN + (!string.IsNullOrEmpty(item.NameEN) ? " " + item.NameEN : null), Convert.ToChar(9));
                tmstr += "\r\n";
                tmstr += string.Format("TMTY:{1}{0}", item.NameType == 3 ? "M" : (item.NameType == 2 ? "D" : "M"), Convert.ToChar(9));
                tmstr += "\r\n";
                foreach (var a in item.ApplicantEN.Split(new char[] { ';', '；' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    tmstr += string.Format("OWNN:{1}{0}", a, Convert.ToChar(9));
                    tmstr += "\r\n";
                }
                tmstr += string.Format("OWNA:{1}{0}", item.AddressEN, Convert.ToChar(9));
                tmstr += "\r\n";
                tmstr += string.Format("OWNC:{0}{1}", Convert.ToChar(9), item.OWNC);
                tmstr += "\r\n";
                tmstr += string.Format("CORN:{1}{0}", item.AgentEN, Convert.ToChar(9));
                tmstr += "\r\n";
                tmstr += "****";
                tmstr += "\r\n";
                //ImageHelper.SaveImage(string.Format("C:\\User\\Alex\\Desktop\\TM1535\\{0}.png", item.PubNumber), item.TMImage);
                FileHelper.WriteToText("C:\\User\\Alex\\Desktop\\TM1535\\EN.txt", tmstr);
                //tms += tmstr;
            }

            //FileHelper.WriteFile("C:\\User\\Alex\\Desktop\\1531\\EN.txt", tms);

        }

        public static void GetTMTrans()
        {
            IList<TMElement> list = Repository.GetTMTrans();
            Repository.SaveTMTrans(list);
        }
    }
}
