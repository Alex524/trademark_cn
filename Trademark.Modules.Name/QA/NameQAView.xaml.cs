﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trademark.Infrastructure;

namespace Trademark.Modules.Name.QA
{
    [ViewExport(RegionName = RegionNames.MainRegion)]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public partial class NameQAView : UserControl
    {
        public NameQAView()
        {
            InitializeComponent();
        }

        [Import]
        public INameQAViewModel Model
        {
            get { return DataContext as INameQAViewModel; }
            set { this.DataContext = value; }
        }
    }
}
