﻿using Prism.Commands;
using Prism.Events;
using Prism.Interactivity.InteractionRequest;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;

namespace Trademark.Modules.Name.QA
{
    [Export(typeof(INameQAViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class NameQAViewModel : BindableBase, INameQAViewModel
    {
        private NameType nameType = NameType.Applicant;
        private bool continueSeach = true;
        private int? publicationNo = null;
        private int? category = null;
        private readonly IEventAggregator EventAggregator;
        public Infrastructure.Models.Name nameModel;
        private ICommand submitCommand;
        private ICommand searchCommand;
        private ICommand rejectCommand;
        private ICommand upperCommand;
        private string reference;

        private readonly ILoginService LoginService;
        private readonly INameFeedService NameFeedService;
        private readonly INameReferenceService ReferenceService;
        private TMItem tm;
        private ICommand qaRejectCommand;

        public InteractionRequest<IConfirmation> ConfirmationRequest { get; private set; }

        public string HeaderInfo
        {
            get
            {
                return "Name QA";
            }
        }

        [ImportingConstructor]
        public NameQAViewModel(IEventAggregator eventAggregator, ILoginService loginService, INameFeedService nameFeedService, INameReferenceService nameReferenceService)
        {
            this.EventAggregator = eventAggregator;
            this.LoginService = loginService;
            this.NameFeedService = nameFeedService;
            this.ReferenceService = nameReferenceService;

            this.searchCommand = new DelegateCommand<string>(Seach);
            this.submitCommand = new DelegateCommand<string>(Submit);
            this.rejectCommand = new DelegateCommand<string>(Reject);
            this.qaRejectCommand = new DelegateCommand<string>(QAReject);
            this.upperCommand = new DelegateCommand<string>(Uper);

            this.NameTypes = new ValueDescriptionList<NameType> {
                new ValueDescription<NameType> { Value = NameType.Applicant, Description = "Applicant" },
                new ValueDescription<NameType> { Value = NameType.Address, Description = "Address" },
                new ValueDescription<NameType> { Value = NameType.Agent, Description = "Agent" },
                new ValueDescription<NameType> { Value = NameType.Description, Description = "Description" },
            };

            this.ConfirmationRequest = new InteractionRequest<IConfirmation>();
        }

        private void QAReject(string obj)
        {
            if (this.NameModel != null)
            {
                this.ConfirmationRequest.Raise(new Confirmation { Content = "Please Confirm to reopen this name to TR", Title = "Confirmation" }, r =>
                {
                    if (r.Confirmed)
                    {
                        NameFeedService.QAReject(this.NameModel);
                        this.Reference = null;
                        this.NameModel = null;
                        if (continueSeach)
                            this.Seach(null);
                    }

                });
            }
        }

        private void Reject(string obj)
        {
            if (this.NameModel != null)
            {
                this.ConfirmationRequest.Raise(new Confirmation { Content = "The Name will be deleted and the referenced trademark will be rejcted to editor, please confirm you want to do this operation?", Title = "Confirmation" }, r =>
                {
                    if (r.Confirmed)
                    {
                        NameFeedService.Reject(this.NameModel);
                        this.Reference = null;
                        this.NameModel = null;
                        if (continueSeach)
                            this.Seach(null);
                    }

                });

            }
        }

        private void Uper(string obj)
        {
            this.NameModel.EN = this.NameModel.EN.ToUpper();
        }

        private void Submit(string obj)
        {
            NameModel.Status = NameModel.Status | 16;
            NameFeedService.Submit(this.NameModel);

            this.Reference = null;
            this.NameModel = null;
            if (continueSeach)
                this.Seach(null);
        }

        private void Seach(string obj)
        {
            if (PublicationNo.HasValue)
            {
                this.NameModel = NameFeedService.GetQAName(PublicationNo, Convert.ToInt32(this.NameType), category, LoginService.GetAccount().Id);
                this.TMModel = null;
                this.Reference = null;
                if (NameModel != null && NameModel.TMID.HasValue)
                {
                    this.TMModel = ReferenceService.GetReference(NameModel.TMID.Value);
                    this.Reference = string.Format("Name ID: {3}{4}Trademark ID: {0}{4}Pub. Number: {1}{4}Page No.: {2}", TMModel.Id, TMModel.PubNumber, TMModel.PageNo, NameModel.ID, Convert.ToChar(9));
                }
            }
        }

        public IValueDescriptionList<NameType> NameTypes { get; set; }

        public NameType NameType
        {
            get { return this.nameType; }
            set
            {
                SetProperty(ref this.nameType, value);
            }
        }

        public int? PublicationNo
        {
            get
            {
                return publicationNo;
            }

            set
            {
                SetProperty(ref this.publicationNo, value);
            }
        }

        public int? Category
        {
            get
            {
                return category;
            }

            set
            {
                SetProperty(ref this.category, value);
            }
        }

        public bool ContinueSeach
        {
            get { return continueSeach; }
            set
            {
                SetProperty(ref continueSeach, value);
            }
        }


        public string Reference
        {
            get
            {
                return reference;
            }

            set
            {
                SetProperty(ref this.reference, value);
            }
        }

        public Infrastructure.Models.Name NameModel
        {
            get { return nameModel; }
            set { SetProperty(ref this.nameModel, value); }
        }

        public Infrastructure.Models.TMItem TMModel
        {
            get { return tm; }
            set { SetProperty(ref tm, value); }
        }

        public ICommand SubmitCommand { get { return this.submitCommand; } }
        public ICommand RejectCommand { get { return this.rejectCommand; } }
        public ICommand QARejectCommand { get { return this.qaRejectCommand; } }
        public ICommand SearchCommand { get { return this.searchCommand; } }
        public ICommand UpperCommand { get { return this.upperCommand; } }
    }
}
