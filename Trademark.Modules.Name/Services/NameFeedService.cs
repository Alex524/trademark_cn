﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Service.Contract;

namespace Trademark.Modules.Name.Services
{
    [Export(typeof(INameFeedService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class NameFeedService : INameFeedService
    {
        public Infrastructure.Models.Name GetQAName(int? pubNo, int? type, int? category, int? userId)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<INameService>();
                var name = service.GetTRName(pubNo.Value, type.Value, userId, category);

                return (name == null) ? null : new Infrastructure.Models.Name
                {
                    ID = name.ID,
                    TMQH = name.TMQH,
                    Type = name.Type,
                    Category = name.Category,
                    CN = name.CN,
                    EN = name.EN,
                    TMID = name.TMID,
                    Status = name.Status,
                    QADate = name.QADate,
                    QAUserID = name.QAUserID,
                    TRDate = name.TRDate,
                    TRUserID = name.TRUserID
                };
            }
        }

        public Infrastructure.Models.Name GetTRName(int? pubNo, int? type, int? category, int? userId)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<INameService>();
                var name = service.GetQAName(pubNo.Value, type.Value, userId, category);

                return (name == null) ? null : new Infrastructure.Models.Name
                {
                    ID = name.ID,
                    TMQH = name.TMQH,
                    Type = name.Type,
                    Category = name.Category,
                    CN = name.CN,
                    EN = name.EN,
                    TMID = name.TMID,
                    Status = name.Status,
                    QADate = name.QADate,
                    QAUserID = name.QAUserID,
                    TRDate = name.TRDate,
                    TRUserID = name.TRUserID
                };
            }
        }

        /// <summary>
        ///fro tr error only, set the name status to "2-TR IN HAND", let them translate it again.
        /// </summary>
        /// <param name="name"></param>
        public void QAReject(Infrastructure.Models.Name name)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<INameService>();
                service.Update(new TMName
                {
                    ID = name.ID,
                    EN = name.EN,
                    Status = name.Status ^ 4
                });
            }
        }

        public void Submit(Infrastructure.Models.Name name)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<INameService>();
                service.Update(new TMName
                {
                    ID = name.ID,
                    EN = name.EN,
                    Status = name.Status
                });
            }
        }

        /// <summary>
        /// qa and tr can use this to reject to editor, while exec this, we will delete the name in the database and set the tm status to rejectted.
        /// </summary>
        /// <param name="name"></param>
        public void Reject(Infrastructure.Models.Name name)
        {
            string nametype = null;
            switch (name.Type)
            {
                case 1:
                    nametype = "Applicant";
                    break;
                case 2:
                    nametype = "Address";
                    break;
                case 3:
                    nametype = "Agent";
                    break;
                case 4:
                    nametype = "Desciption";
                    break;
            }

            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<INameService>();
                service.Reject(new TMName
                {
                    ID = name.ID,
                    Reason = string.Format("The \"{0}\" with content \"{1}\" have been reject by name system", nametype, name.CN)
                });
            }
        }
    }
}
