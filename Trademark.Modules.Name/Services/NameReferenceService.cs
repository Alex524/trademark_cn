﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure;
using Trademark.Infrastructure.Interfaces;
using Trademark.Infrastructure.Models;
using Trademark.Service.Contract;

namespace Trademark.Modules.Name.Services
{
    [Export(typeof(INameReferenceService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class NameReferenceService : INameReferenceService
    {
        public TMItem GetReference(int id)
        {
            using (ESBClient client = new ESBClient())
            {
                var service = client.GetClientProxy<ITrademarkService>();
                var tm = service.GetById(id);

                return new TMItem(tm);
            }
        }
    }
}
