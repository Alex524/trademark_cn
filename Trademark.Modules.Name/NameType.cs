﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trademark.Modules.Name
{
    public enum NameType : int
    {
        Applicant = 1,
        Address = 2,
        Agent = 3,
        Description = 4
    }
}
