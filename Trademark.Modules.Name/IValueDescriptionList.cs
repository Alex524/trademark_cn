

using System.Collections.Generic;

namespace Trademark.Modules.Name
{
    public interface IValueDescriptionList<T> : IList<ValueDescription<T>> where T : struct
    {

    }
}
