﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trademark.Infrastructure.Interfaces;

namespace Trademark.Modules.Name
{
    public interface INameTRViewModel : IHeaderInfoProvider<string>
    {
    }
}
